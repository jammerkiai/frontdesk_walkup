<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsThemeExist = mysql_query("SELECT * FROM themes WHERE theme_name = '".addslashes(trim($_POST['theme_name']))."'");
	$rownum_rsThemeExists = mysql_num_rows($rsThemeExist);
	if ($rownum_rsThemeExists == 0) {
		$insertSQL = sprintf("INSERT INTO themes (theme_name) VALUES (%s)",GetSQLValueString($_POST['theme_name'], "text"));
		$Result1 = mysql_query($insertSQL) or die(mysql_error());
	} 
	else {
		echo "<script>alert('Theme name already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$theme_array = array();
	$theme_array = explode("|",$_POST['theme_array']);
	foreach($theme_array as $key => $value) {
		if (isset($_POST['theme_name_'.$value]) && $_POST['theme_name_'.$value] != "") { 
			$updateSQL = sprintf("UPDATE themes SET theme_name=%s WHERE theme_id=%s",
				GetSQLValueString($_POST['theme_name_'.$value], "text"), GetSQLValueString($value, "int"));
  			$Result2 = mysql_query($updateSQL) or die(mysql_error());
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: theme_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (theme_name LIKE '".addslashes($_POST['search'])."%' OR theme_name LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsTheme = 10;
$pageNum_rsTheme = 0;
if (isset($_GET['pageNum_rsTheme'])) {
  $pageNum_rsTheme = $_GET['pageNum_rsTheme'];
}
$startRow_rsTheme = $pageNum_rsTheme * $maxRows_rsTheme;
$param_rsTheme = " WHERE 1=1 ".$param_search;

//$query_rsTheme = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsTheme, $sortDate);
$query_rsTheme = sprintf("select * from themes %s ", $param_rsTheme);
$query_limit_rsTheme = sprintf("%s LIMIT %d, %d", $query_rsTheme, $startRow_rsTheme, $maxRows_rsTheme);
$rsTheme = mysql_query($query_limit_rsTheme) or die(mysql_error());
$row_rsTheme = mysql_fetch_assoc($rsTheme);

if (isset($_GET['totalRows_rsTheme'])) {
  $totalRows_rsTheme = $_GET['totalRows_rsTheme'];
} else {
  $all_rsTheme = mysql_query($query_rsTheme);
  $totalRows_rsTheme = mysql_num_rows($all_rsTheme);
}
$totalPages_rsTheme = ceil($totalRows_rsTheme/$maxRows_rsTheme)-1;

$queryString_rsTheme = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsTheme") == false && 
        stristr($param, "totalRows_rsTheme") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsTheme = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsTheme = sprintf("&totalRows_rsTheme=%d%s", $totalRows_rsTheme, $queryString_rsTheme);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsTheme + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsTheme + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

?>
<html>
<head>
<title>Theme</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="theme.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">THEME</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Theme Name:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','theme_name','#q','0','Field Theme name is required.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="3%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="90%"><strong style="color:#678197;">Theme Name</strong></td>
                </tr></thead>
                <?php
					$themeArray = ""; 
					if ($totalRows_rsTheme > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$themeArray .= $row_rsTheme['theme_id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsTheme['theme_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsTheme['theme_id']; ?></td>
				  <td align="left"><input type="text" name="theme_name_<?=$row_rsTheme['theme_id']?>" value="<?php echo $row_rsTheme['theme_name']; ?>" class="textbox-style"></td>
                </tr>
                <?php } while ($row_rsTheme = mysql_fetch_assoc($rsTheme)); ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="theme_array" value="<?=substr($themeArray,0,-1)?>">&nbsp;</td><td><input name="theme_name" id="theme_name" type="text" class="textbox-style" ><script>document.getElementById('theme_name').focus()</script></td></tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsTheme > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsTheme > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsTheme=%d%s", $currentPage, 0, $queryString_rsTheme); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsTheme=%d%s", $currentPage, max(0, $pageNum_rsTheme - 1), $queryString_rsTheme); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsTheme) {
    printf('<a href="'."%s?pageNum_rsTheme=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsTheme.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsTheme < $totalPages_rsTheme) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsTheme=%d%s", $currentPage, min($totalPages_rsTheme, $pageNum_rsTheme + 1), $queryString_rsTheme); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsTheme=%d%s", $currentPage, $totalPages_rsTheme, $queryString_rsTheme); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsTheme == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>