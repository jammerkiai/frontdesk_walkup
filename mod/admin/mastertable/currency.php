<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsCurrencyExist = mysql_query("SELECT * FROM currency WHERE currency_name = '".addslashes(trim($_POST['currency_name']))."'");
	$rownum_rsCurrencyExists = mysql_num_rows($rsCurrencyExist);
	if ($rownum_rsCurrencyExists == 0) {
		$insertSQL = sprintf("INSERT INTO currency (currency_name, currency_value) VALUES (%s,%s)",GetSQLValueString($_POST['currency_name'], "text"),GetSQLValueString($_POST['currency_value'], "double"));
		$Result1 = mysql_query($insertSQL) or die(mysql_error());
	} 
	else {
		echo "<script>alert('Currency Name already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$currency_array = array();
	$currency_array = explode("|",$_POST['currency_array']);
	foreach($currency_array as $key => $value) {
		if ((isset($_POST['currency_name_'.$value]) && $_POST['currency_name_'.$value] != "") && (isset($_POST['currency_value_'.$value]) && $_POST['currency_value_'.$value] != "")) {
			$updateSQL = sprintf("UPDATE currency SET currency_name=%s, currency_value=%s WHERE id=%s",
				GetSQLValueString($_POST['currency_name_'.$value], "text"), 
				GetSQLValueString($_POST['currency_value_'.$value], "double"), 
				GetSQLValueString($value, "int"));
  			$Result2 = mysql_query($updateSQL) or die(mysql_error());
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: currency_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (currency_name LIKE '".addslashes($_POST['search'])."%' OR currency_name LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsCurrency = 10;
$pageNum_rsCurrency = 0;
if (isset($_GET['pageNum_rsCurrency'])) {
  $pageNum_rsCurrency = $_GET['pageNum_rsCurrency'];
}
$startRow_rsCurrency = $pageNum_rsCurrency * $maxRows_rsCurrency;
$param_rsCurrency = " WHERE 1=1 ".$param_search;

//$query_rsCurrency = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsCurrency, $sortDate);
$query_rsCurrency = sprintf("select * from currency %s ", $param_rsCurrency);
$query_limit_rsCurrency = sprintf("%s LIMIT %d, %d", $query_rsCurrency, $startRow_rsCurrency, $maxRows_rsCurrency);
$rsCurrency = mysql_query($query_limit_rsCurrency) or die(mysql_error());
$row_rsCurrency = mysql_fetch_assoc($rsCurrency);

if (isset($_GET['totalRows_rsCurrency'])) {
  $totalRows_rsCurrency = $_GET['totalRows_rsCurrency'];
} else {
  $all_rsCurrency = mysql_query($query_rsCurrency);
  $totalRows_rsCurrency = mysql_num_rows($all_rsCurrency);
}
$totalPages_rsCurrency = ceil($totalRows_rsCurrency/$maxRows_rsCurrency)-1;

$queryString_rsCurrency = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsCurrency") == false && 
        stristr($param, "totalRows_rsCurrency") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsCurrency = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsCurrency = sprintf("&totalRows_rsCurrency=%d%s", $totalRows_rsCurrency, $queryString_rsCurrency);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsCurrency + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsCurrency + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

?>
<html>
<head>
<title>Currency</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="currency.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">CURRENCY</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Currency:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','currency_name','#q','0','Field Currency Name is required.','currency_value','#1_999','1','Field Currency Value is required and must be a number.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="4%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="4%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="46%"><strong style="color:#678197;">Currency</strong></td>
				   <td align="left" width="46%"><strong style="color:#678197;">Value</strong>&nbsp;<font style="font-size:10px;">(Equivalent US Dollar)</font></td>
                </tr></thead>
                <?php
					$currencyArray = ""; 
					if ($totalRows_rsCurrency > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$currencyArray .= $row_rsCurrency['id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsCurrency['id']; ?>"></td>
                  <td align="left"><?php echo $row_rsCurrency['id']; ?></td>
				  <td align="left"><input type="text" name="currency_name_<?=$row_rsCurrency['id']?>" value="<?php echo $row_rsCurrency['currency_name']; ?>" class="textbox-style-2"></td>
				  <td align="left"><input type="text" name="currency_value_<?=$row_rsCurrency['id']?>" value="<?php echo $row_rsCurrency['currency_value']; ?>" class="textbox-style-2" ></td>
                </tr>
                <?php } while ($row_rsCurrency = mysql_fetch_assoc($rsCurrency)); ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="currency_array" value="<?=substr($currencyArray,0,-1)?>">&nbsp;</td><td><input name="currency_name" id="currency_name" type="text" class="textbox-style-2" ><script>document.getElementById('currency_name').focus()</script></td>
				  <td><input name="currency_value" id="currency_value" type="text" class="textbox-style-2" ></td>
				</tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsCurrency > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsCurrency > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsCurrency=%d%s", $currentPage, 0, $queryString_rsCurrency); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsCurrency=%d%s", $currentPage, max(0, $pageNum_rsCurrency - 1), $queryString_rsCurrency); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsCurrency) {
    printf('<a href="'."%s?pageNum_rsCurrency=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsCurrency.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsCurrency < $totalPages_rsCurrency) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsCurrency=%d%s", $currentPage, min($totalPages_rsCurrency, $pageNum_rsCurrency + 1), $queryString_rsCurrency); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsCurrency=%d%s", $currentPage, $totalPages_rsCurrency, $queryString_rsCurrency); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsCurrency == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>