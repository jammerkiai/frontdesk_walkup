<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$error = "";
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
 	$dow="";
	if (isset($_POST['dow_monday'])) $dow.=$_POST['dow_monday']."|"; 
	if (isset($_POST['dow_tuesday'])) $dow.=$_POST['dow_tuesday']."|"; 
	if (isset($_POST['dow_wednesday'])) $dow.=$_POST['dow_wednesday']."|"; 
	if (isset($_POST['dow_thursday'])) $dow.=$_POST['dow_thursday']."|";
	if (isset($_POST['dow_friday'])) $dow.=$_POST['dow_friday']."|"; 
	if (isset($_POST['dow_saturday'])) $dow.=$_POST['dow_saturday']."|";
	if (isset($_POST['dow_sunday'])) $dow.=$_POST['dow_sunday']."|";
	$dow=substr($dow,0,-1);
	$rsRoomExist = mysql_query("SELECT rate_name FROM rates_dow WHERE rate_name = '".trim($_POST['rate_name'])."'");
	$rownum_rsRoomExists = mysql_num_rows($rsRoomExist);
	if ($rownum_rsRoomExists == 0) {
	  $insertSQL = sprintf("INSERT INTO rates_dow (rate_name, hour_start, hour_end, dow, duration, display) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['rate_name'], "text"),
					   GetSQLValueString($_POST['hour_start'], "int"),
					   GetSQLValueString($_POST['hour_end'], "int"),
					   GetSQLValueString($dow, "text"),
					   GetSQLValueString($_POST['duration'], "int"),
					   GetSQLValueString($_POST['display'], "text"));

 	 $Result1 = mysql_query($insertSQL) or die(mysql_error());

 	 $insertGoTo = "rates_add.php?strMsg=Record has been successfully added.";
	  if (isset($_SERVER['QUERY_STRING'])) {
 	   $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
 	   $insertGoTo .= $_SERVER['QUERY_STRING'];
 	 }
 	 header(sprintf("Location: %s", $insertGoTo));
  }
  else echo "<script>alert('* Rate Name already Exists.')</script>";
} 
else if ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:rates.php");
} # end insert validate

?>
<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add Rates</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="rates_add" id="rates_add" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
    <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
    <tr>
    	<td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
    </tr>
    <?php } ?>
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top">
				  <td colspan="2" align="center" class="bgHeader"><strong>ADD RATES</strong></td>
				</tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">Rate Name  </td>
                  <td><input name="rate_name" type="text" class="textbox-style" id="rate_name" style="width:300px;" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Hour Start</td>
                  <td><select name='hour_start' id='hour_start' style="width:200px;" class="textbox-style">
					<option value='0'></option>
					<option value='24' >12AM</option>
					<option value='1' >1AM</option>
					<option value='2' >2AM</option>
					<option value='3' >3AM</option>
					<option value='4' >4AM</option>
					<option value='5' >5AM</option>
					<option value='6' >6AM</option>
					<option value='7' >7AM</option>
					<option value='8' >8AM</option>
					<option value='9' >9AM</option>
					<option value='10' >10AM</option>
					<option value='11' >11AM</option>
					<option value='12' >12PM</option>
					<option value='13' >1PM</option>
					<option value='14' >2PM</option>
					<option value='15' >3PM</option>
					<option value='16' >4PM</option>
					<option value='17' >5PM</option>
					<option value='18' >6PM</option>
					<option value='19' >7PM</option>
					<option value='20' >8PM</option>
					<option value='21' >9PM</option>
					<option value='22' >10PM</option>
					<option value='23' >11PM</option>
					</select>
</td>
                </tr>
								<tr align="left" valign="top">
                  <td align="left">Hour Start</td>
                  <td><select name='hour_end' id='hour_end' style="width:200px;" class="textbox-style" >
					<option value='0'></option>
					<option value='24' >12AM</option>
					<option value='1' >1AM</option>
					<option value='2' >2AM</option>
					<option value='3' >3AM</option>
					<option value='4' >4AM</option>
					<option value='5' >5AM</option>
					<option value='6' >6AM</option>
					<option value='7' >7AM</option>
					<option value='8' >8AM</option>
					<option value='9' >9AM</option>
					<option value='10' >10AM</option>
					<option value='11' >11AM</option>
					<option value='12' >12PM</option>
					<option value='13' >1PM</option>
					<option value='14' >2PM</option>
					<option value='15' >3PM</option>
					<option value='16' >4PM</option>
					<option value='17' >5PM</option>
					<option value='18' >6PM</option>
					<option value='19' >7PM</option>
					<option value='20' >8PM</option>
					<option value='21' >9PM</option>
					<option value='22' >10PM</option>
					<option value='23' >11PM</option>
					</select>
</td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Days of Week</td>
                  <td>&nbsp;<input type='checkbox' name='dow_monday' id='dow_monday'  value='Monday'>Monday<br />&nbsp;<input type='checkbox' name='dow_tuesday' id='dow_tuesday'  value='Tuesday'>Tuesday<br />&nbsp;<input type='checkbox' name='dow_wednesday' id='dow_wednesday'  value='Wednesday'>Wednesday<br />&nbsp;<input type='checkbox' name='dow_thursday' id='dow_thursday'  value='Thursday'>Thursday<br />&nbsp;<input type='checkbox' name='dow_friday' id='dow_friday'  value='Friday'>Friday<br />&nbsp;<input type='checkbox' name='dow_saturday' id='dow_saturday'  value='Saturday'>Saturday<br />&nbsp;<input type='checkbox' name='dow_sunday' id='dow_sunday'  value='Sunday'>Sunday<br /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Duration</td>
                  <td><input type='text'  name='duration' id='duration' ></td>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Display</td>
                  <td><input type='radio' name='display' id='display'  checked  value='Yes'>Yes&nbsp;<input type='radio' name='display' id='display'  value='No'>No</td>
                </tr>
 
                <tr align="left" valign="top">
                  <td>&nbsp;</td>
                  <td align="right"><input name="save" type="submit" class="buttons" id="Save" onclick="YY_checkform('rates_add','display[0]','#q','2','Field Display is required.','display[1]','#q','2','Field Display is required.','rate_name','#q','0','Field Rate Name is required.','duration','#1_24','1','Field Duration is required and must be a number.','hour_start','#q','1','Field Hour Start is required.','hour_end','#q','1','Field Hour End is required.');return document.MM_returnValue" value="Save" />
                  &nbsp;<input name="back" type="submit" id="Back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
