<?php
//checkout.php
session_start();
include_once("config/config.inc.php");
include_once("reporting.php");

if($_POST["act"]=='paybalance') { //process payment
	$cash = $_POST["newcash"];
	$card = $_POST["newcard"];
	$change = $_POST["newchange"] * (-1);
	$occupancy=$_POST["occupancy"];
	$now = date("Y-m-d H:i:s");
		//payments
	if($cash) {
		$sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount) 
			values ('$now', '$occupancy', 'Cash', '$cash')";
		mysql_query($sql) or die($sql . mysql_error());
	}

	if($change) {
		$sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount) 
			values ('$now', '$occupancy', 'Change', '$change')";
		mysql_query($sql) or die($sql . mysql_error());
	}
	
	if($card) {
		$trxnid=$_POST["newtrxn"];
		$batch=$_POST["newbatch"];
		$cardtype = $_POST["ctype"];
		$sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount) 
			values ('$now', '$occupancy', 'Card', '$card')";
		mysql_query($sql) or die($sql . mysql_error());
		//insert card details
		$newsalesid = mysql_insert_id();
		$sql = " insert into card_payment_details (salesreceipt_id, cardtype, transaction_number, batch_number) 
				values ('$newsalesid','$cardtype','$trxnid','$batch')";
		mysql_query($sql) or die($sql . mysql_error());
	}
}elseif($_POST["act"]=='checkout'){
	$occupancy=$_POST["occupancy"];
	$room = $_POST["roomid"];
	$user = $_SESSION["hotel"]["userid"];
	//update occupancy
	$now = date("Y-m-d H:i:s");
	$sql = "update occupancy set actual_checkout='$now', update_by='$user' 
			where occupancy_id='$occupancy'";
	mysql_query($sql) or die($sql . mysql_error());
	
	//add occupancy log
	$sql = " insert into occupancy_log(transaction_date, occupancy_id, update_by, remarks ) value ('$now', '$occupancy', '$user', 'CheckOut' ) ";
	mysql_query($sql) or die($sql . mysql_error());
	
	//update room status
	$sql = " update rooms set status=3, last_update='$now',update_by='$user' where room_id='$room' ";
	mysql_query($sql) or die($sql . mysql_error());
}

$room =($_GET["roomid"]) ? $_GET["roomid"] : $_POST["roomid"];
$obj = new Reporting($room);


if($obj->occupied > 0) {
	

	
	
	$retval= "
		<h1>Statement of Account</h1>
		<br />
		<table width='300'>
		<tr>
		<td>Room</td><td>$obj->doorname </td>
		<td>Check-in</td><td>$obj->checkindate </td>
		</tr>
		<tr>
		<td>Rate</td><td>$obj->ratename </td>
		<td>Exp Checkout</td><td>$obj->etd</td>
		</tr>
		<tr>
		<td>Shift</td><td>$obj->shift </td>
		<td>&nbsp;</td><td>&nbsp;</td>
		</tr>
		</table>";
	
	$retval.= $obj->getRoomCharges();
	$retval.= $obj->getCheckoutFnbSales();
	$retval.= $obj->getCheckoutMosSales();
	$retval.= $obj->getPayments();
	$retval.= $obj->displayTotal();
	$retval.= $obj->displayTotalBalance();
	
	
}else{
	echo "Not checked in.";
	exit;
}
?>
<style>
h1 { text-align:center;font-size:1em;font-family:verdana, arial, helvetica;}
table {font-family:lucida,arial,helvetica}
table td {font-size:.6em} 
table th {font-size:.7em;font-weight:bold;text-align:left;}
.x-footer{bottom:0;left:10;font-size:.6em}

#soatable td{
	border-bottom:1px dotted #cccccc;
}

#cmdmenu {
	list-style:none;
	margin-left:-40px;
}

#cmdmenu li{
	float:left;
	width:100px;
}

.cmdbtn {
	width:100px;
	border:1px solid  #ffffff;
	background-color: #9BD1E6;
	margin-right:2px;
	padding:2px;
	cursor:pointer;
}
.money {
	width:40;
	text-align:right;
}
#paymenttable {
	font-family:lucida, arial, helvetica;
}
#paymenttable td {
	font-weight:normal;font-size:.7em;
}

#paymenttable td.orange {
	color:#ff6600;font-weight:bold;
}

#paymenttable td.change {
	color:#007700;font-weight:bold;
}

</style>
<form name='mycheckouform'  id="mycheckoutform" method='post'>
<input type="hidden" name="roomid"  id="roomid" value="<?=$room?>" />
<input type="hidden" name="occupancy"  id="occupancy" value="<?=$obj->occupancy?>" />
<input type="hidden" name="act"  id="act" value="" />
<table>
<tr><td>
<?php echo $retval ?>
</td><td>&nbsp;</td><td valign='bottom'  align='right'>
<?php

if($obj->totalbalance != 0 ) {
	echo "<fieldset ><legend style='font-size:1.2em;font-weight:bold;border:1px solid #cccccc;background-color:#efefef;padding:2px;color:#9BD1E6;'>Balance Payment</legend>
	<table id='paymenttable' width='240'>";
	echo "<tr><td  class='orange'>Please pay this amount</td><td><input type='text'  id='newbalance' name='newbalance' value='$obj->totalbalance' class='money' /></td></tr>";
	echo "<tr><td>Cash Tendered</td><td><input type='text' name='newcash' id='newcash' value='0' class='money' /></td></tr>";
	echo "<tr><td>Card Payment</td><td><input type='text' name='newcard' value='0'  id='newcard' class='money' /></td></tr>";
	echo "<tr><td colspan=2>
		<fieldset><legend>Card Payment Details<legend>
		<table>
		<tr><td>
		Select card type
		</td><td>
		<input type='radio' name='ctype' value='Visa' id='ctype1' /><label for='ctype1'>Visa</label><br />
		<input type='radio' name='ctype' value='Mastercard' id='ctype2' /><label for='ctype2'>Mastercard</label></td></tr>
		<tr><td><label for=''>Trans#</label></td><td><input type='text' name='newtrxn' ></td></tr>
		<tr><td><label for=''>Batch#</label></td><td><input type='text' name='newbatch' ></td></tr>
		</table>
		</fieldset>
	</td></tr>";
	echo "<tr><td class='change'>Total Change</td><td><input type='text' id='newchange' value='$change' name='newchange' value='0'  class='money' /></td></tr>";
	echo "</table></legend>";
	echo "<ul id='cmdmenu'>";
	echo '<li><input type="button" name="cmdbtn" id="cmdbtn1" value="Re-compute"  class="cmdbtn" /></li>';
	echo '<li><input type="button" name="cmdbtn" id="cmdbtn2" value="Pay Balance"  class="cmdbtn" /></li>';
	echo "</ul>";
}else{
	echo "<ul id='cmdmenu'>";
	echo '<li><input type="button" name="cmdbtn" id="cmdbtn3" value="Print"  class="cmdbtn" /></li>';
	echo '<li><input type="button" name="cmdbtn" id="cmdbtn4" value="Checkout"  class="cmdbtn" /></li>';
	echo "</ul>";
}
?>
</td></tr>
</table>
</form>
<script type='text/javascript' src='../js/jquery.js'></script>
<script lang="javascript">
	function recompute() {
		var bal =  $("#newbalance").val() * 1;
		var cash = $("#newcash").val() * 1;
		var card = $("#newcard").val() ;
		var pay = cash + card * 1;
		$("#newchange").val( pay- bal) ;
	}
	$(document).ready(function(){
		$("#newcash").change(recompute);
		$("#newcard").change(recompute);
		$("#cmdbtn1").click(recompute);
		$("#cmdbtn2").click(function(){
			recompute;
			var change = $("#newchange").val() ;
			//if(change != 0) {
			//	alert('You need to complete payment to be able to checkout.');
			//	return false;
			//}else{
				$("#act").val("paybalance");
				document.getElementById('mycheckoutform').submit();
			//}
		});
		$("#cmdbtn3").click(function() {
			 window.print();
		});
		
		$("#cmdbtn4").click(function() {
			 $("#act").val("checkout");
			document.getElementById('mycheckoutform').submit();
			window.parent.location.href="../index.php";
		});
	});
</script>