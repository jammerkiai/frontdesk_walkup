<?php

require "./config/config.inc.php";

if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (guest_fname LIKE '".addslashes($_POST['search'])."%' OR guest_lname LIKE '%".addslashes($_POST['search'])."%' OR remarks LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsUsers = 10;
$pageNum_rsUsers = 0;
if (isset($_GET['pageNum_rsUsers'])) {
  $pageNum_rsUsers = $_GET['pageNum_rsUsers'];
}
$startRow_rsUsers = $pageNum_rsUsers * $maxRows_rsUsers;
$param_rsUsers = " WHERE 1=1 ".$param_search;

//$query_rsUsers = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsUsers, $sortDate);
$query_rsUsers = sprintf("select * from security_receivables %s ", $param_rsUsers);
$query_limit_rsUsers = sprintf("%s LIMIT %d, %d", $query_rsUsers, $startRow_rsUsers, $maxRows_rsUsers);
$rsUsers = mysql_query($query_limit_rsUsers) or die(mysql_error());
$row_rsUsers = mysql_fetch_assoc($rsUsers);

if (isset($_GET['totalRows_rsUsers'])) {
  $totalRows_rsUsers = $_GET['totalRows_rsUsers'];
} else {
  $all_rsUsers = mysql_query($query_rsUsers);
  $totalRows_rsUsers = mysql_num_rows($all_rsUsers);
}
$totalPages_rsUsers = ceil($totalRows_rsUsers/$maxRows_rsUsers)-1;

$queryString_rsUsers = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsUsers") == false && 
        stristr($param, "totalRows_rsUsers") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsUsers = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsUsers = sprintf("&totalRows_rsUsers=%d%s", $totalRows_rsUsers, $queryString_rsUsers);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsUsers + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsUsers + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

$now = date("Y-m-d H:i:s");

if($_POST["cmdRemit"]=="Remit security receivable")
{
	foreach($_POST["rec_id"] as $sr_id)
	{
		$_sr_id .= $sr_id . ",";
	}
	
	$sql = " update security_receivables set date_remitted = '$now' where sr_id in (".substr_replace($_sr_id ,"",-1).") ";
	mysql_query($sql) or die($sql . mysql_error());
	
}


?>
<html>
<head>
<title>Security Receivables</title>
<script type="text/javascript" src="../js/custom.js"></script>
<link href="../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">RECEIVABLES</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Receivables:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdRemit" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to remit a security receivable(s). Are you sure you want to continue?');return document.MM_returnValue" value="Remit security receivable" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="20%"><strong style="color:#678197;">Guest Name</strong></td>
				  <td align="left" valign="middle" width="20%"><strong style="color:#678197;">Date Endorsed</strong></td>
				  <td align="left" valign="middle" width="20%"><strong style="color:#678197;">Date Remitted</strong></td>
				  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Remarks</strong></td>
                </tr></thead>
                <?php if ($totalRows_rsUsers > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsUsers['sr_id']; ?>"></td>
				  <td align="left"><?php echo $row_rsUsers['guest_fname']." ".$row_rsUsers['guest_lname']; ?></td>
                  <td align="left"><?php echo date("m/d/y - g:i A", strtotime($row_rsUsers['date_endorsed'])); ?></td>
				  <td align="left"><?php if($row_rsUsers['date_remitted']=='0000-00-00 00:00:00') echo "Not remitted yet"; else echo date("m/d/y - g:i A", strtotime($row_rsUsers['date_remitted'])); ?></td>				 
				  <td align="center"><?php echo $row_rsUsers['guest_fname'] ?> <?php echo $row_rsUsers['guest_lname'] ?><br />
				  <?php echo $row_rsUsers['remarks'] ?>
				  </td>
                </tr>
                <?php } while ($row_rsUsers = mysql_fetch_assoc($rsUsers)); ?>
                <?php } // Show if recordset not empty ?>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsUsers > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsUsers > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, 0, $queryString_rsUsers); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, max(0, $pageNum_rsUsers - 1), $queryString_rsUsers); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsUsers) {
    printf('<a href="'."%s?pageNum_rsUsers=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsUsers.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsUsers < $totalPages_rsUsers) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, min($totalPages_rsUsers, $pageNum_rsUsers + 1), $queryString_rsUsers); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, $totalPages_rsUsers, $queryString_rsUsers); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsUsers == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>