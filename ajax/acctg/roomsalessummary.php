<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

/******************************/
// edit room count here
$WPROOMS = 69;
$HSROOMS = 116;
$TOTALROOMS = $WPROOMS + $HSROOMS;
/******************************/

$title = 'Room Sales Summary by Type';
$thismonth = isset($_POST['thismonth']) ? $_POST['thismonth'] : date('m');
$thisyear = isset($_POST['thisyear']) ? $_POST['thisyear'] : date('Y');

if($_POST["submit"]=="export to excel") {
	$table="<h3 style='font-size:12px;font-family:arial,helvetica'>$title - $thismonth/$thisyear</h3>";
	$table.="<table border='0' cellspacing='2' cellpadding='2' style='font-size:11px;font-family:arial,helvetica'>";
	$table.=getHeaders();
	$table.=getData($thisyear,$thismonth);
	$table.="</table>";
	$excel_file_name="mss_$thisyear_$thismonth.xls";
	header("Content-type: application/octet-stream");//A MIME attachment with the content type "application/octet-stream" is a binary file.
	header("Content-Disposition: attachment; filename=$excel_file_name");//with this extension of file name you tell what kind of file it is.
	header("Pragma: no-cache");//Prevent Caching
	header("Expires: 0");//Expires and 0 mean that the browser will not cache the page on your hard drive
	echo $table;
	exit;
}

function getHeaders() {
	$retval = "<tr>";
	$retval.="<th>Date</th>";
	$retval.="<th>Room Type</th>";
	$retval.="<th>Sold/Occupied</th>";
	$retval.="<th>Available</th>";
	$retval.="<th>Occupancy Rate</th>";
	$retval.="<th>Room Sales</th>";
	$retval.="<th>Average Room Sales</th>";
	$retval .= "</tr>";
	return $retval;
}

function getData($thisyear,$thismonth) {
	$sql = "select a.salesdate, a.roomtypename, sum(1) as rooms_occupied, 
(select count(c.door_name) from room_types b, rooms c 
where b.room_type_name=a.roomtypename and b.room_type_id=c.room_type_id) as room_count, 
(sum(1)/(select count(c.door_name) from room_types b, rooms c 
where b.room_type_name=a.roomtypename and b.room_type_id=c.room_type_id)) as occupancy_rate,
sum(a.roomsales) as total_room_sales_per_type, format((sum(a.roomsales)/sum(1)), 2) as avg_roomrate_per_type
from salesdetailsummary a
where year(salesdate)='$thisyear' and month(salesdate)='$thismonth'
group by a.salesdate, a.roomtypename
order by a.salesdate desc, a.roomtypename";
	$res = mysql_query($sql);
	$thisDay = '';
	$dailyRoomsOccupied = 0;
	$dailyRoomsAvailable = 0;
	$dailyTotalSales = 0;
	while ($row = mysql_fetch_row($res)) {
		if ($thisDay != $row[0]) {
			//do daily summary data
			if ($thisDay != '') {
				$occRate = number_format(($dailyRoomsOccupied/$dailyRoomsAvailable), 2);
				$avgDailyRate = number_format(($dailyTotalSales/$dailyRoomsOccupied), 2);
				$rows.='<tr><th colspan=3></th>';
				$rows.="<th>Occupancy Rate</th><th>$occRate</th>";
				$rows.="<th>Average Daily Room Rate</th><th>$avgDailyRate</th>";
				$rows.='</tr><tr><th colspan=7></th></tr>';
				$dailyRoomsOccupied = 0;
				$dailyRoomsAvailable = 0;
				$dailyTotalSales = 0;
			}
			$thisDay = $row[0];
		} else {
			$dailyRoomsOccupied += $row[2];
			$dailyRoomsAvailable += $row[3];
			$dailyTotalSales += $row[5];
		}
		$rows .= "<tr>";
		foreach($row as $fieldvalue) {
			$rows .= "<td>$fieldvalue</td>";
		}
		$rows .= "</tr>";
		
	}
	return $rows;
}

function getMonth($selected) {
	$ret = "<select name='thismonth' id='thismonth'>";
	for($x=1; $x <=12; $x++) {
		$month = date('F', strtotime('2010-' . $x . '-01'));
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$month</option>";
	}
	$ret.="</select>";
	return $ret;
}

function getYear($selected) {
	$ret = "<select name='thisyear' id='thisyear'>";
	for($x=2010; $x <=2015; $x++) {
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$x</option>";
	}
	$ret.="</select>";
	return $ret;
}

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
<style>
table {
	border-collapse:collapse;
}
table th,td {
	padding:4px;
	text-align:center;
}

th.grand, td.grand {
	background-color:#eeffcc;
}

tr.weekend {
	background-color:#ffeecc;
}
</style>
</head>
<body>
<form method='post'>
<div>
Select Month: <?php echo getmonth($thismonth) . getyear($thisyear) ?>
<input type="submit" name="submit" value="go" />
<input type="submit" name="submit" value="export to excel" />
</div>
<div id="workpanel">

<table border='1'>
<?php
echo getHeaders();
echo getData($thisyear,$thismonth);
?>
</table>
</div>
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>

