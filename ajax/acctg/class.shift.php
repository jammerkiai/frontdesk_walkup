<?php
class shift extends baseobject
{
	public function __construct($params) 
	{
		parent::init($params);
		if(!isset($params['shiftno'])) {
			$h = date("H");
			if($h>=16&&$h<=23)
			{
				$this->shiftno=3;
			}
			elseif($h>=8&&$h<=15)
			{
				$this->shiftno=2;
			}
			$this->shiftno=1;
		}
	}
	
	public function getShiftDuration()
	{
		$sql = "select shift_start, shift_end from shifts where shift_id=".$this->shiftno;
		$res = mysql_query($sql);
		list($this->hrStart,$this->hrEnd) = mysql_fetch_row($res);
		$this->shiftStart = $this->date . ' ' . $this->hrStart . ':01:00';
		$this->shiftEnd = date('Y-m-d H:i:s', strtotime($this->date . ' ' . $this->hrEnd . ':01:00'));
	}
}