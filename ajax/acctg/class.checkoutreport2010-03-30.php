<?php
/**
* checkout report class extends class.report.php
*/

class checkoutreport 
{	
	public function checkoutreport()
	{
		$this->getRates();
	}
	
	public function getStartTime()
	{
		$sql = "select datetime from `shift-transactions` where shift = 'start' order by datetime desc";
		$res = mysql_query($sql);
		while(list($time) = mysql_fetch_row($res))
		{
			return $time;
		}	
	}
	
	public function getshift($date) {
		if(!$date)$date=date("Y-m-d H:i:s");
		list($d, $t) = explode(" ", $date);
		list($h, $m, $s) = explode(":", $t);
		$sql = "select shift_id from shifts where $h between shift_start and shift_end";
		$res = mysql_query($sql) or die(mysql_error() .$sql);
		list($shift)=mysql_fetch_row($res);
		
		if($h==16||$h==15)
		{
			$this->shiftnum=3;
			return $shift = "3rd";
		}
		elseif($h==8||$h==7)
		{
			$this->shiftnum=2;
			return $shift = "2nd";
		}
		$this->shiftnum=1;
	return  "1st";
	}
	
	public function getLatestShifts($shiftid)
	{
		$sql = "SELECT `shift-transaction_id`,datetime,user_id  FROM `shift-transactions` where shift = 'start' order by datetime desc ";
		$res = mysql_query($sql);
		$ret = "<select name='rblshifts' id='rblshifts' onchange='myform.submit();'>";
		$ret .= "<option value=''>&nbsp;</option>";
		while(list($shift_transaction_id,$datetime,$userid)=mysql_fetch_row($res))
		{
			if($shiftid == $shift_transaction_id)
			{
				$select = "selected";
			}else
			{
				$select = " ";
			}
			$__sql = "SELECT user_id  FROM `shift-transactions` where shift = 'end' 
			and `datetime` > '$datetime'
			order by datetime asc 
			limit 0,1";
			$__res = mysql_query($__sql);
			list($userid)=mysql_fetch_row($__res);
			$_sql = "select fullname from users where user_id = '$userid'";
			$_res = mysql_query($_sql);
			list($username)=mysql_fetch_row($_res);
			$ret .= "<option value='$shift_transaction_id' $select>$datetime - $username - ".$this->getshift($datetime)."</option>";
		}
		$ret .= "</select>";

		return $ret;
	}

	
	public function getSpecialFloorID() 
	{
		$sql = " select settings_value from settings where settings_name = 'SPECIALFLOORID' ";
		$res = mysql_query($sql) or die($sql);
		
		if(mysql_num_rows($res)){
			$row = mysql_fetch_row($res);
			$this->intSpecialFloorId = $row[0];
		} 
	}
	
	public function getSpecialRoomIdList()
	{
		//echo "getSpecialRoomIdList<br>";
		$this->getSpecialFloorId();
		$sql = " select room_id from rooms where floor_id=".$this->intSpecialFloorId;
		
		$res = mysql_query($sql) or die($sql);
		while(list($id)=mysql_fetch_row($res)) {
			$this->arrSpecialRooms[]=$id;
		}
	}
	
	public function getSpecialDetail()
	{
		$this->getSpecialRoomidList();
		$headers=array('Room Type','Food','Beer','Misc','Adjust','Deduct','Discount','Total');
		$retval.='<table class="report">';
		$retval.='<tr>';
		foreach($headers as $header){
			$retval.="<th>$header</th>";
		}
		$retval.='</tr>';
		
		foreach($this->arrSpecialRooms as $roomid) {
			$occupancy = $this->getOccupancy($roomid);
			if($occupancy) $retval.=$this->getSpecialDetailPerOccupancy($occupancy);
		}
		$retval.='<tr class="aggregates">';
		$retval.='<th>Totals: </th>';
		foreach($this->totals['specials'] as $total) {
			$retval.="<th>$total</th>";
		}
		$retval.='</tr>';
		$retval.='</table>';
		return $retval;
	}
	
	public function getSpecialDetailPerOccupancy($occupancy)
	{
		$arrRoom = $this->getRoomDetailsByOccupancy($occupancy);
		$food = $this->getFoodSalesByOccupancy($occupancy, $this->start, $this->end, array(),array(17,21), 1,0,0,0 );
		$beer = $this->getFoodSalesByOccupancy($occupancy, $this->start, $this->end, array(17,21),array(), 1 ,0,0,0);
		$misc = $this->getRoomSalesByOccupancy($occupancy, $this->start, $this->end, array(),array(2,3), 'category_id', 1,0,0,0 );
		$adjust = $this->getRoomSalesByOccupancy($occupancy, $this->start, $this->end, array(18),array(), 'item_id',1,0,0,0 );
		$deduct = $this->getRoomSalesByOccupancy($occupancy, $this->start, $this->end, array(27),array(), 'item_id',1,0,0,0 );
		$discount = $this->getRoomSalesByOccupancy($occupancy, $this->start, $this->end, array(17),array(), 'item_id',1,0,0,0 );
		$total = $food + $beer + $misc + $adjust + $deduct + $discount;
		$this->totals['specials']['food']+=$food;
		$this->totals['specials']['beer']+=$beer;
		$this->totals['specials']['misc']+=$misc;
		$this->totals['specials']['adjust']+=$adjust;
		$this->totals['specials']['deduct']+=$deduct;
		$this->totals['specials']['discount']+=$discount;
		$this->totals['specials']['total']+=$total;
		$retval = '<tr>';
		$retval.='<td>'. $arrRoom['door_name'] .'</td>';
		$retval.='<td>'. $food .'</td>';
		$retval.='<td>'. $beer .'</td>';
		$retval.='<td>'. $misc .'</td>';
		$retval.='<td>'. $adjust .'</td>';
		$retval.='<td>'. $deduct .'</td>';
		$retval.='<td>'. $discount .'</td>';
		$retval.='<td>'. $total .'</td>';
		$retval.= '</tr>';
		return $retval;
	}
	
	public function getOccupancy($roomid) 
	{
		$sql  =" select occupancy_id from occupancy where room_id=$roomid order by occupancy_id desc ";
		$res = mysql_query($sql) or die($sql);
		if(mysql_num_rows($res)) {
			$row = mysql_fetch_array($res);
			return $row[0];
		}
		return 0;
	}
	
	public function getRoomDetailsByOccupancy($occupancy) 
	{
		$sql  =" select rooms.* from occupancy, rooms where occupancy.room_id=rooms.room_id and occupancy.occupancy_id='$occupancy'";
		$res = mysql_query($sql) or die($sql);
		$row = mysql_fetch_array($res);
		return $row;
	}
	
	public function getFoodSalesByOccupancy($occupancy, $start='', $end='', $in=array(),$out=array(),$site=1,$los=0, $days=0,$duration=0) 
	{
		if(count($in)) {
			$arrIn = implode(',',$in);
			$whereIn = " and category_id in ($arrIn) ";
		}
		if(count($out)) {
			$arrOut = implode(',',$out);
			$whereIn = " and category_id not in ($arrOut) ";
		}
		if($site==2) {
			$newstart = date('Y-m-d H:i:s',strtotime("$start - $duration hours"));
			if($days > 0) {
				$daterange = " and sales_date >='$newstart'
					and sales_date <='$end' ";
				
			}else{
				if ($los > 1) {
					$daterange = " and sales_date >='$newstart'
						and sales_date <='$end' ";
				}
			}
		}elseif($site==1){
			if($start !='') $add=" and sales_date >= '$start' "; 
		    if($end !='') $add.=" and sales_date <= '$end' ";
		}
		$_sql = "select sum(unit_cost*qty) from fnb_sales 
				where status in ('Paid')
				$whereIn $whereOut
				$daterange
				$add
				and occupancy_id = '$occupancy'";
		//echo "$occupancy $_sql<hr>";
		
		$_res = mysql_query($_sql) or die($sql);
		list($value) = mysql_fetch_row($_res);
		return $value;
	}
	
	public function getRoomRateByOccupancy($occupancy, $start='', $end='' ,$site=1,$los=0, $days=0,$duration=0,$shiftnum=1)
	{
		//set options later for rechit here
		if ($site==1) {
			$sql = "select unit_cost*qty from room_sales
					where occupancy_id=$occupancy
					and item_id=15
					and status='Paid'
					order by sales_date desc limit 1
				   ";
		} elseif($site==2) {
			$newstart = date('Y-m-d H:i:s',strtotime("$start - $duration hours"));
			if($days > 0) {
				$daterange = " and sales_date >='$newstart'
					and sales_date <='$end' ";
				
			}else{
				if ($los > 1) {
					$daterange = " and sales_date >='$newstart'
						and sales_date <='$end' ";
				}
			}
			$sql = "select unit_cost*qty from room_sales
					where occupancy_id=$occupancy
					and item_id=15
					and status='Paid'
					$daterange
					order by sales_date desc 
				   ";
		}
		//echo "$occupancy $los $days $duration $sql<hr>";
		$res = mysql_query($sql);
		$row = mysql_fetch_row($res);
		return $row[0];
	}
	
	public function getRoomCheckinExtensionByOccupancy($occupancy, $start='', $end='' ,$site=1,$los=0, $days=0,$duration=0,$shiftnum=1)
	{
		if ($site==1) {
			$sql = "select a.unit_cost*a.qty, a.roomsales_id
				FROM room_sales a, occupancy_log b
				WHERE a.occupancy_id = b.occupancy_id
				And a.sales_date=b.transaction_date
				AND a.item_id = '16'
				AND b.remarks not in ('Overtime on checkout')
				AND a.status in ('Paid')
				AND a.occupancy_id = '$occupancy'";
		} elseif($site==2) {
			//$newstart = date('Y-m-d H:i:s',strtotime("$start - $duration hours"));
			if($days > 0) {
				$daterange = " and update_date >='$start'
					and update_date <='$end' ";
			}else{
				if ($los > 1) {
					$daterange = " and update_date >='$start'
						and update_date <='$end' ";
				}
			}
			$sql = "select a.unit_cost*a.qty, a.roomsales_id
				FROM room_sales a, occupancy_log b
				WHERE a.occupancy_id = b.occupancy_id
				And a.sales_date=b.transaction_date
				AND a.item_id = '16'
				AND b.remarks not in ('Overtime on checkout')
				AND a.status in ('Paid')
				$daterange
				AND a.occupancy_id = '$occupancy'";
		}
		//echo "$occupancy $sql<hr>";
		$res = mysql_query($sql);
		$row = mysql_fetch_row($res);
		
		return $row[0];
	}
	
	public function getRoomCheckoutOTByOccupancy($occupancy, $start='', $end='' ,$site=1,$los=0, $days=0,$duration=0,$shiftnum=1)
	{
		if($site==2) {
			$newstart = date('Y-m-d H:i:s',strtotime("$start - $duration hours"));
			if($days > 0) {
				$daterange = " and a.sales_date >='$start'
					and a.sales_date <='$end' ";
				
			}else{
				if ($los > 1) {
					$daterange = " and a.sales_date >='$start'
						and a.sales_date <='$end' ";
				}
			}
		}
		$sql = "select a.roomsales_id
			FROM room_sales a, occupancy_log b
			WHERE a.occupancy_id = b.occupancy_id
			And a.sales_date=b.transaction_date
			AND a.item_id = '16'
			AND b.remarks not in ('Overtime on checkout')
			AND a.status in ('Paid')
			$daterange
			AND a.occupancy_id = '$occupancy'";
		$res = mysql_query($sql);

		if(mysql_num_rows($res)) {
			list($inextid) = mysql_fetch_row($res);
		}
		
		$sql = "select sum(a.unit_cost*a.qty)
			FROM room_sales a, occupancy_log b
			WHERE a.occupancy_id = b.occupancy_id
			AND a.item_id = '16'
			AND b.remarks in ('Overtime on checkout')
			and a.roomsales_id not in ('$inextid')
			$daterange
			AND a.occupancy_id = '$occupancy'";
		//echo "$occupancy $site $days $los $duration shiftnum $sql<hr> ";
		$res = mysql_query($sql);
		$row = mysql_fetch_row($res);
		return $row[0];
	}
	
	public function getRoomSalesByOccupancy($occupancy, $start, $end, $in=array(),$out=array(),$key='category_id',$site=1,$los=0, $days=0,$duration=0,$shiftnum=1)
	{
		if(count($in)) {
			$arrIn = implode(',',$in);
			$whereIn = " and $key in ($arrIn) ";
		}
		if(count($out)) {
			$arrOut = implode(',',$out);
			$whereIn = " and $key not in ($arrOut) ";
		}
		if($site==2) {
			$newstart = date('Y-m-d H:i:s',strtotime("$start - $duration hours"));
			if($days > 0) {
				$daterange = " and sales_date >='$newstart'
					and sales_date <='$end' ";
				
			}else{
				if ($los > 1) {
					$daterange = " and sales_date >='$newstart'
						and sales_date <='$end' ";
				}
			}
		}elseif($site==1){
			if($start !='') $add=" and sales_date >= '$start' "; 
			if($end !='') $add.=" and sales_date <= '$end' ";
		}
		$_sql = "select sum(unit_cost*qty) from room_sales 
				where status in ('Paid')
				$whereIn $whereOut  $add
				$daterange
				and occupancy_id = '$occupancy' ";
		
		$_res = mysql_query($_sql) or die($_sql);
		
		//echo "$occupancy $site $los $days $duration $_sql <hr>";
		
		list($value) = mysql_fetch_row($_res);
		//if($arrIn=='17') echo $_sql."<hr>";
		return $value;
	}	
	
	public function getWalkupDetail()
	{
		$exceptList = implode(',',$this->arrSpecialRooms);
		$sql = "select * from occupancy a, rooms b, room_types c, rates d 
				where a.actual_checkout >= '$this->start' 
				and a.actual_checkout <= '$this->end' 
				and a.room_id = b.room_id
				and b.room_type_id=c.room_type_id
				and a.rate_id=d.rate_id
				and b.site_id=1
				and b.room_id not in ($exceptList)
				order by b.room_type_id asc,
				a.rate_id desc,
				a.actual_checkout asc";
		$res = mysql_query($sql);			

		while( $row=mysql_fetch_array($res) ) {
			$retval.=$this->getWalkinDetailPerOccupancy($row);
		}
		
		if(isset($this->totals['walkin'])) {
			$retval.='<tr class="aggregates">';
			$retval.='<th>Totals: </th>';
			foreach($this->totals['walkin'] as $total) {
				if(is_numeric($total)) $total = number_format($total,2);
				$retval.="<th>$total</th>";
			}
			$retval.='</tr>';
		}
		
		
		return $retval;
	}
	
	public function getRates()
	{
		$sql = " select rate_id,rate_name from rates limit 3";
		$res = mysql_query($sql);
		while($row = mysql_fetch_row($res)) {
			$this->rates[$row[0]]=$row[1];
		}
	}
	
	public function getWalkinDetailPerOccupancy($row)
	{
		$occupancy = $row['occupancy_id'];
		$doorname = $row['door_name'];
		$ratename = $row['duration'];
		$roomtypename = $row['room_type_name'];
		
		$food = $this->getFoodSalesByOccupancy($occupancy, '', '', array(),array(17,21) );
		$beer = $this->getFoodSalesByOccupancy($occupancy, '', '', array(17,21),array() );
		$misc = $this->getRoomSalesByOccupancy($occupancy, '', '', array(),array(2,3) );
		$adjust = $this->getRoomSalesByOccupancy($occupancy, '', '', array(18),array(),'item_id' );
		$deduct = $this->getRoomSalesByOccupancy($occupancy, '', '', array(27),array(),'item_id' );
		$discount = $this->getRoomSalesByOccupancy($occupancy, '', '', array(17),array(),'item_id' );
		$roomrate = $this->getRoomRateByOccupancy($occupancy);
		$in_ot = $this->getRoomCheckinExtensionByOccupancy($occupancy);
		$out_ot = $this->getRoomCheckoutOTByOccupancy($occupancy);
		
		$in_total=$in_ot + $roomrate + $out_ot + $discount + $deduct ;
		
		$total = $food + $beer + $misc + $in_total+ $adjust;
		
		$this->totals['walkin']['doorname']+=1;
		$this->totals['walkin']['ratename']+=1;
		$this->totals['walkin']['roomrate']+=$roomrate;
		$this->totals['walkin']['ext']+=$in_ot;
		$this->totals['walkin']['discount']+=$discount;
		$this->totals['walkin']['in_total']+=$in_total;
		$this->totals['walkin']['chkin']='&nbsp;';
		$this->totals['walkin']['chkout']='&nbsp;';
		$this->totals['walkin']['food']+=$food;
		$this->totals['walkin']['beer']+=$beer;
		$this->totals['walkin']['misc']+=$misc;
		$this->totals['walkin']['adjust']+=$adjust;
		$this->totals['walkin']['deduct']+=$deduct;
		$this->totals['walkin']['out_ot']+=$out_ot;
		$this->totals['walkin']['total']+=$total;

		$this->summary[$roomtypename][$row['rate_name']]+=1;
		$this->summary[$roomtypename]['room']+=$in_ot + $out_ot + $roomrate + $discount;
		$this->summary[$roomtypename]['ot']+=$in_ot + $out_ot;
		$this->summary[$roomtypename]['food']+=$food;
		$this->summary[$roomtypename]['beer']+=$beer;
		$this->summary[$roomtypename]['misc']+=$misc;
		$this->summary[$roomtypename]['adjust']+=$adjust;
		$this->summary[$roomtypename]['deduct']+=$deduct;
		$this->summary[$roomtypename]['sales_total']+=$total;

		$retval = '<tr>';
		$retval.='<td>'. $roomtypename .'</td>';
		$retval.='<td>'. $doorname .'</td>';
		$retval.='<td>'. $ratename .'</td>';
		$retval.='<td>'. $roomrate .'</td>';
		$retval.='<td>'. $in_ot .'</td>';
		$retval.='<td>'. $discount .'</td>';
		$retval.='<td>'. $in_total .'</td>';
		$retval.='<td nowrap>'. date('m/d H:i',strtotime($row['actual_checkin'])) .'</td>';
		$retval.='<td nowrap>'. date('m/d H:i',strtotime($row['actual_checkout'])) .'</td>';
		$retval.='<td>'. $food .'</td>';
		$retval.='<td>'. $beer .'</td>';
		$retval.='<td>'. $misc .'</td>';
		$retval.='<td>'. $adjust .'</td>';
		$retval.='<td>'. $deduct .'</td>';
		$retval.='<td>'. $out_ot .'</td>';
		$retval.='<td>'. $total .'</td>';
		$retval.='<td class="debug"><a href="occupancydetails.php?occ='.$row['occupancy_id'].'" target="_blank">'. $row['occupancy_id'] .'</a></td>';
		$retval.= '</tr>';
		return $retval;
	}
	
	public function getHotelDetail()
	{
		$exceptList = implode(',',$this->arrSpecialRooms);
		$cutoff = date('Y-m-d H:i:s', strtotime($this->start . ' -24 hours'));
		$sql = "drop table if exists temphoteldetails;";
		mysql_query($sql);
		$sql = "
				create table temphoteldetails
				select a.occupancy_id,a.actual_checkin,a.actual_checkout,
				b.site_id,b.door_name,b.room_id,c.room_type_id, c.room_type_name,c.rank
				,d.rate_id,d.rate_name,d.duration, a.shift_checkin,
				if(actual_checkout='0000-00-00 00:00:00',timestampdiff(HOUR,a.actual_checkin,now())/d.duration,
				if(actual_checkout > '$this->end',timestampdiff(HOUR,a.actual_checkin,'$this->end')/d.duration, '0')) as 'status',
				if(actual_checkout='0000-00-00 00:00:00',timestampdiff(HOUR,a.actual_checkin,now())/d.duration,
				if(actual_checkout > '$this->end',timestampdiff(HOUR,a.actual_checkin,'$this->end')/d.duration, 
				timestampdiff(HOUR,a.actual_checkin,a.actual_checkout)/d.duration)) as 'los'
				from occupancy a, rooms b, room_types c, rates d 
				where 
				(
				(a.actual_checkout = '0000-00-00 00:00:00' and a.actual_checkin < '$this->start' and a.shift_checkin=$this->shiftnum)
				or (a.actual_checkin < '$this->start' and actual_checkout > '$this->end'  and a.shift_checkin=$this->shiftnum)
				or (a.actual_checkout >= '$this->start' and a.actual_checkout <= '$this->end' )
				)
				and a.room_id = b.room_id
				and b.room_type_id=c.room_type_id
				and a.rate_id=d.rate_id
				and b.site_id=2
				and a.room_id not in ($exceptList)
				";

		mysql_query($sql) or die(mysql_error());		
		$sql = " select * from temphoteldetails
				order by rank asc, room_type_id asc,
				rate_id desc,
				actual_checkout asc";
		
		$res = mysql_query($sql) or die($sql);			

		while( $row=mysql_fetch_array($res) ) {
			$retval.=$this->getHotelDetailPerOccupancy($row);
		}
		if(isset($this->totals['hotel'])) {
			$retval.='<tr class="aggregates">';
			$retval.='<th>Totals: </th>';
			foreach($this->totals['hotel'] as $total) {
				$retval.="<th>$total</th>";
			}
			$retval.='</tr>';
		}
		
		return $retval;
	}
	
	public function getHotelDetailPerOccupancy($row)
	{
		/*
		* summaries
		$this->summary['roomtypename']['occupancy_total']=
			$this->summary['roomtypename']['3hrs'] + 
			$this->summary['roomtypename']['12hrs'] + 
			$this->summary['roomtypename']['24hrs']
		$this->summary['roomtypename']['sales_total'] =
			$this->summary['roomtypename']['Room']
			$this->summary['roomtypename']['Ot']
			$this->summary['roomtypename']['Food']
			$this->summary['roomtypename']['beer']
			$this->summary['roomtypename']['misc']
			$this->summary['roomtypename']['adj']
			$this->summary['roomtypename']['deduct']
		
		*/
		$occupancy = $row['occupancy_id'];
		$doorname = $row['door_name'];
		$ratename = $row['duration'];
		$roomtypename = $row['room_type_name'];
		
		$food = $this->getFoodSalesByOccupancy($occupancy, $this->start, $this->end, array(),array(17,21),$row['site_id'],ceil($row['los']),floor($row['status']), $ratename );
		$beer = $this->getFoodSalesByOccupancy($occupancy, $this->start, $this->end, array(17,21),array(),$row['site_id'],ceil($row['los']),floor($row['status']), $ratename );

		$misc = $this->getRoomSalesByOccupancy($occupancy, '', '', array(),array(2,3),'category_id',$row['site_id'],ceil($row['los']),floor($row['status']), $ratename  );
		$adjust = $this->getRoomSalesByOccupancy($occupancy, '', '', array(18),array(),'item_id',$row['site_id'],ceil($row['los']),floor($row['status']), $ratename  );
		$deduct = $this->getRoomSalesByOccupancy($occupancy, '', '', array(27),array(),'item_id',$row['site_id'],ceil($row['los']),floor($row['status']), $ratename  );
		$discount = $this->getRoomSalesByOccupancy($occupancy, '', '', array(17),array(),'item_id',$row['site_id'],ceil($row['los']),floor($row['status']), $ratename  );

		$roomrate = $this->getRoomRateByOccupancy($occupancy, $this->start, $this->end,$row['site_id'],ceil($row['los']),floor($row['status']),$ratename,$row['shift_checkin']);
		$in_ot = $this->getRoomCheckinExtensionByOccupancy($occupancy, $this->start, $this->end,$row['site_id'],ceil($row['los']),floor($row['status']),$ratename,$row['shift_checkin']);
		$out_ot = $this->getRoomCheckoutOTByOccupancy($occupancy, $this->start, $this->end,$row['site_id'],ceil($row['los']),floor($row['status']),$ratename,$row['shift_checkin']);
		
		$in_total=$in_ot + $roomrate + $out_ot + $discount + $deduct;		
		$total = $food + $beer + $misc + $adjust + $in_total;
		
		$this->totals['hotel']['doorname']+=1;
		$this->totals['hotel']['ratename']+=1;
		$this->totals['hotel']['roomrate']+=$roomrate;
		$this->totals['hotel']['ext']+=$in_ot;
		$this->totals['hotel']['discount']+=$discount;
		$this->totals['hotel']['in_total']+=$in_total;
		$this->totals['hotel']['chkin']='&nbsp;';
		$this->totals['hotel']['chkout']='&nbsp;';
		$this->totals['hotel']['food']+=$food;
		$this->totals['hotel']['beer']+=$beer;
		$this->totals['hotel']['misc']+=$misc;
		$this->totals['hotel']['adjust']+=$adjust;
		$this->totals['hotel']['deduct']+=$deduct;
		$this->totals['hotel']['out_ot']+=$out_ot;
		$this->totals['hotel']['total']+=$total;

		
		$this->summary[$roomtypename][$row['rate_name']]+=1;
		$this->summary[$roomtypename]['room']+=$in_ot + $out_ot + $roomrate + $discount;
		$this->summary[$roomtypename]['ot']+=$in_ot + $out_ot;
		$this->summary[$roomtypename]['food']+=$food;
		$this->summary[$roomtypename]['beer']+=$beer;
		$this->summary[$roomtypename]['misc']+=$misc;
		$this->summary[$roomtypename]['adjust']+=$adjust;
		$this->summary[$roomtypename]['deduct']+=$deduct;
		$this->summary[$roomtypename]['sales_total']+=$total;

		$retval = '<tr>';
		$retval.='<td>'. $roomtypename .'</td>';
		$retval.='<td>'. $doorname .'</td>';
		$retval.='<td>'. $ratename .'</td>';
		$retval.='<td>'. $roomrate .'</td>';
		$retval.='<td>'. $in_ot .'</td>';
		$retval.='<td>'. $discount .'</td>';
		$retval.='<td>'. $in_total .'</td>';
		$retval.='<td nowrap>'. date('m/d H:i',strtotime($row['actual_checkin'])) .'</td>';
		if(floor( $row['status'] )) {
			$retval.='<td nowrap>'. floor( $row['status'] ) .'</td>';
		}else{
			$retval.='<td nowrap>'. date('m/d H:i',strtotime($row['actual_checkout'])) .'</td>';
		}
		$retval.='<td>'. $food .'</td>';
		$retval.='<td>'. $beer .'</td>';
		$retval.='<td>'. $misc .'</td>';
		$retval.='<td>'. $adjust .'</td>';
		$retval.='<td>'. $deduct .'</td>';
		$retval.='<td>'. $out_ot .'</td>';
		$retval.='<td>'. $total .'</td>';
		
		$retval.='<td class="debug"><a href="occupancydetails.php?occ='.$row['occupancy_id'].'" target="_blank">'. $row['occupancy_id'] .'</a></td>';
		$retval.= '</tr>';
		return $retval;
	}
	
	public function getCheckoutDetail($startshift,$end,$suser_id,$euser_id)
	{
		$this->start=$startshift;
		$this->end=$end;
		$retval= $this->getReportHeader($this->start,$this->end,$suser_id,$euser_id);
		$retval.=$this->getSpecialDetail();
		$headers=array('Rm_Type','Rm_No','HRS','Rate','Ext','Disc','InTot',
		'ChkIn','ChkOut',
		'Food','Beer','Misc','Adjust','Deduct','OT','Total');
		$retval.='<table class="report">';
		$retval.='<tr>';
		foreach($headers as $header){
			if(is_array($header)) {
				foreach($header as $key=>$value) {
					$retval.="<th>$value</th>";
				}
			}else{
				$retval.="<th>$header</th>";
			}
		}
		$retval.='</tr>';
		
		$retval.=$this->getWalkupDetail();
		$retval.=$this->getHotelDetail();		
		$retval.='</table>';
		echo  $retval;
	}
	
	public function getCheckoutSummary($startshift,$end,$suser_id,$euser_id)
	{
		$this->start=$startshift;
		$this->end=$end;
		$retval=$this->getReportHeader($this->start,$this->end,$suser_id,$euser_id,'CHECKOUT SUMMARY');
		$retval.='<table class="report">';
		$retval.='<tr>';
		$headers=array('','#TOTAL',$this->rates,'ROOM',
		'OT','FOOD','BEER','MISC','ADJUST','DEDUCT','TOTAL');
		foreach($headers as $header){
			if(is_array($header)) {
				foreach($header as $key=>$value) {
					$retval.="<th>$value</th>";
				}
			}else{
				$retval.="<th>$header</th>";
			}
		}
		$retval.='</tr>';
		$retval.=$this->getWalkupSummary($this->start,$this->end);
		$retval.=$this->getHotelSummary($this->start,$this->end);		
		$retval.='</table>';
		echo $retval;
	}
	
	public function getWalkupSummary($start,$end)
	{
		$sql  = "select room_type_id, room_type_name 
				 from room_types 
				 where site_id = 1 
				 order by rank 
				 ";
		$res = mysql_query($sql);
		$this->walkupsummary=array();
		while(list($roomtypeid,$roomtypename)=mysql_fetch_row($res)) {
			$retval.="<tr>";
			$retval.="<td>$roomtypename</td>";
			$count=array();
			$counttotal=0;
			foreach($this->rates as $rateid => $ratename) {
				$ratecount = (isset($this->summary[$roomtypename][$ratename])) ? $this->summary[$roomtypename][$ratename] : 0;
				$counttotal+=$ratecount;
				$counts[$ratename]=$ratecount;
			}
			$retval.= '<td>'. $counttotal . '</td>';
			$this->walkupsummary['allcounttotal']+=$counttotal;
			foreach ($counts as $key => $value) {
				$retval.= '<td>'. $value . '</td>';
				$this->walkupsummary[$key]+=$value;
			}
			$room = (isset($this->summary[$roomtypename]['room'])) ? $this->summary[$roomtypename]['room'] : '';
			$ot = (isset($this->summary[$roomtypename]['ot'])) ? $this->summary[$roomtypename]['ot'] : '';
			$food = (isset($this->summary[$roomtypename]['food'])) ? $this->summary[$roomtypename]['food'] : '';
			$beer = (isset($this->summary[$roomtypename]['beer'])) ? $this->summary[$roomtypename]['beer'] : '';
			$misc = (isset($this->summary[$roomtypename]['misc'])) ? $this->summary[$roomtypename]['misc'] : '';
			$adjust= (isset($this->summary[$roomtypename]['adjust'])) ? $this->summary[$roomtypename]['adjust'] : '';
			$deduct = (isset($this->summary[$roomtypename]['deduct'])) ? $this->summary[$roomtypename]['deduct'] : '';
			$sales_total = (isset($this->summary[$roomtypename]['sales_total'])) ? $this->summary[$roomtypename]['sales_total'] : 0;

			$this->walkupsummary['room']+=$room;
			$this->walkupsummary['ot']+=$ot;
			$this->walkupsummary['food']+=$food;
			$this->walkupsummary['beer']+=$beer;
			$this->walkupsummary['misc']+=$misc;
			$this->walkupsummary['adjust']+=$adjust;
			$this->walkupsummary['deduct']+=$deduct;
			$this->walkupsummary['sales_total']+=$sales_total;

			$retval.= '<td>'. $room . '</td>';
			$retval.= '<td>'. $ot . '</td>';
			$retval.= '<td>'. $food . '</td>';
			$retval.= '<td>'. $beer . '</td>';
			$retval.= '<td>'. $misc . '</td>';
			$retval.= '<td>'. $adjust . '</td>';
			$retval.= '<td>'. $deduct . '</td>';
			$retval.= '<td>'. $sales_total . '</td>';
			$retval.= '</tr>';
		}
		if( isset($this->walkupsummary) ){
			$retval.="<tr class='aggregates'>";
			$retval.='<th>SubTotals:</th>';
			foreach($this->walkupsummary as $key=>$value) {
				$retval.="<th>$value</th>";
			}
			$retval.="</tr>";
		}
		return $retval;
	}
	
	public function getTotalRoomCostByRoomType($roomtypeid,$start,$end,$in=array(),$out=array(),$key='category_id')
	{
		if(count($in)) {
			$arrIn = implode(',',$in);
			$whereIn = " and b.{$key} in ($arrIn) ";
		}
		if(count($out)) {
			$arrOut = implode(',',$out);
			$whereIn = " and b.{$key} not in ($arrOut) ";
		}
		$_sql = "select sum(unit_cost*qty) from occupancy a, room_sales b,  rooms c
			where b.status in ('Paid')
			and a.occupancy_id = b.occupancy_id	
			
			and a.room_id = c.room_id
			$whereIn $whereOut
			and c.room_type_id = '$roomtypeid' ";
		if($start !='') $_sql.=" and a.actual_checkout >= '$start' "; 
		if($end !='') $_sql.=" and a.actual_checkout <= '$end' ";
		$_res = mysql_query($_sql) or die($_sql);
		
		
		list($value) = mysql_fetch_row($_res);
		//if($arrIn=='15') echo $_sql."<hr>";
		return $value;
	}
	
	public function getTotalRoomRateByRoomType()
	{
		
	}
	
	public function getTotalFoodCostByRoomType($roomtypeid,$start,$end,$in=array(),$out=array(),$key='category_id')
	{
		if(count($in)) {
			$arrIn = implode(',',$in);
			$whereIn = " and b.{$key} in ($arrIn) ";
		}
		if(count($out)) {
			$arrOut = implode(',',$out);
			$whereIn = " and b.{$key} not in ($arrOut) ";
		}
		$_sql = "select sum(unit_cost*qty) from occupancy a, fnb_sales b,  rooms c
			where b.status in ('Paid')
			and a.occupancy_id = b.occupancy_id	
			and a.room_id = c.room_id
			$whereIn $whereOut
			and c.room_type_id = '$roomtypeid' ";
		if($start !='') $_sql.=" and a.actual_checkout >= '$start' "; 
		if($end !='') $_sql.=" and a.actual_checkout <= '$end' ";
		$_res = mysql_query($_sql) or die($_sql);
		
		
		list($value) = mysql_fetch_row($_res);
		//if($arrIn=='17') echo $_sql."<hr>";
		return $value;
	}
	
	public function getRateCountByRoomType($roomtypeid,$rateid='',$start,$end)
	{
		$sql = "select count(a.occupancy_id) from  occupancy a,rooms b
		where a.room_id = b.room_id
		and a.actual_checkout >= '$start'
		and a.actual_checkout <= '$end'
		and b.room_type_id = '$roomtypeid'";
		if($rateid!='') $sql .= " and a.rate_id='$rateid' ";
		
		$res = mysql_query($sql);
		$row = mysql_fetch_row($res);
		return $row[0];
	}
	
	public function getHotelSummary($start,$end)
	{
		$sql  = "select room_type_id, room_type_name 
				 from room_types 
				 where site_id = 2 
				 order by rank 
				 ";
		$res = mysql_query($sql);
		$this->hotelsummary=array();
		while(list($roomtypeid,$roomtypename)=mysql_fetch_row($res)) {
			$retval.="<tr>";
			$retval.="<td>$roomtypename</td>";
			$count=array();
			$counttotal=0;
			foreach($this->rates as $rateid => $ratename) {
				$ratecount = (isset($this->summary[$roomtypename][$ratename])) ? $this->summary[$roomtypename][$ratename] : 0;
				$counttotal+=$ratecount;
				$counts[$ratename]=$ratecount;
			}
			$retval.= '<td>'. $counttotal . '</td>';
			$this->hotelsummary['allcounttotal']+=$counttotal;
			foreach ($counts as $key => $value) {
				$retval.= '<td>'. $value . '</td>';
				$this->hotelsummary[$key]+=$value;
			}
			$room = (isset($this->summary[$roomtypename]['room'])) ? $this->summary[$roomtypename]['room'] : '';
			$ot = (isset($this->summary[$roomtypename]['ot'])) ? $this->summary[$roomtypename]['ot'] : '';
			$food = (isset($this->summary[$roomtypename]['food'])) ? $this->summary[$roomtypename]['food'] : '';
			$beer = (isset($this->summary[$roomtypename]['beer'])) ? $this->summary[$roomtypename]['beer'] : '';
			$misc = (isset($this->summary[$roomtypename]['misc'])) ? $this->summary[$roomtypename]['misc'] : '';
			$adjust= (isset($this->summary[$roomtypename]['adjust'])) ? $this->summary[$roomtypename]['adjust'] : '';
			$deduct = (isset($this->summary[$roomtypename]['deduct'])) ? $this->summary[$roomtypename]['deduct'] : '';
			$sales_total = (isset($this->summary[$roomtypename]['sales_total'])) ? $this->summary[$roomtypename]['sales_total'] : 0;

			$this->hotelsummary['room']+=$room;
			$this->hotelsummary['ot']+=$ot;
			$this->hotelsummary['food']+=$food;
			$this->hotelsummary['beer']+=$beer;
			$this->hotelsummary['misc']+=$misc;
			$this->hotelsummary['adjust']+=$adjust;
			$this->hotelsummary['deduct']+=$deduct;
			$this->hotelsummary['sales_total']+=$sales_total;

			$retval.= '<td>'. $room . '</td>';
			$retval.= '<td>'. $ot . '</td>';
			$retval.= '<td>'. $food . '</td>';
			$retval.= '<td>'. $beer . '</td>';
			$retval.= '<td>'. $misc . '</td>';
			$retval.= '<td>'. $adjust . '</td>';
			$retval.= '<td>'. $deduct . '</td>';
			$retval.= '<td>'. $sales_total . '</td>';
			$retval.= '</tr>';
		}
		if( isset($this->hotelsummary) ){
			$retval.="<tr class='aggregates'>";
			$retval.='<th>SubTotals:</th>';
			foreach($this->hotelsummary as $key=>$value) {
				$retval.="<th>$value</th>";
			}
			$retval.="</tr>";
		}
		return $retval;
	}
	
	public function getReportHeader($start,$end,$suser_id,$euser_id,$type='CHECKOUT')
	{
		$sql = "select settings_value from settings where id = '1'";
		$res = mysql_query($sql);
		list($value) = mysql_fetch_row($res);
		$ret.="<div class='report'>";
		$ret.= "<b>".strtoupper($value)."</b><br>";
		$ret.= "<b>ROOM $type REPORT</b><br>";
		$ret.= "<b>SHIFT: </b>".$this->getshift($start). "&nbsp;&nbsp;&nbsp;&nbsp;".date("m/d/Y g:i:s A",strtotime($start));
		$ret.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>CASHIER: </b>";
		if($suser_id == $euser_id)
		{
			$_sql = "select fullname from users where user_id = '$euser_id'";
			$_res = mysql_query($_sql);
			list($cashier)=mysql_fetch_row($_res);
			$ret.=$cashier;
		}else
		{
			$_sql = "select fullname from users where user_id = '$suser_id'";
			$_res = mysql_query($_sql);
			$ret.=$scashier;
			list($scashier)=mysql_fetch_row($_res);

			$ret.=" - ";

			$_sql = "select fullname from users where user_id = '$euser_id'";
			$_res = mysql_query($_sql);
			list($ecashier)=mysql_fetch_row($_res);
			$ret.=$ecashier;
		}
		
		
		$ret.= "<br>";
		$ret.="</div>";
		$ret.= "<br>";

		return $ret;
	}

	public function getCoopSummary($startshift,$end,$suser_id,$euser_id) {
		include_once('acctg/class.baseobject.php');
		include_once('acctg/class.report.php');
		$sql= "
		select room_types.room_type_name as 'RM_TYPE', rooms.door_name as 'RM_NO', room_sales.sales_date as 'SALES DATE', 
		occupancy.actual_checkin as 'CHECKIN', occupancy.actual_checkout as 'CHECKOUT', sales_and_services.sas_description as 'ITEM', 
		room_sales.unit_cost as 'UNIT', room_sales.qty as 'QTY', (room_sales.unit_cost * room_sales.qty) as 'TOTAL COST'
		from room_sales, sas_category, sales_and_services, occupancy, rooms, room_types
		where room_sales.category_id=sas_category.sas_cat_id
		and room_sales.item_id=sales_and_services.sas_id
		and room_sales.occupancy_id=occupancy.occupancy_id
		and occupancy.room_id=rooms.room_id
		and rooms.room_type_id=room_types.room_type_id
		and room_sales.sales_date >= '$startshift'
		and room_sales.sales_date <= '$end'
		and occupancy.actual_checkout <= '$end'
		and room_sales.category_id=2
		order by rooms.door_name
		";

		$arrReport = array(
			'title'    => 'Shogun 1 - Cooperative Sales',
			'aggregates'=> array('TOTAL COST'),
			'subtitle' => date('l, F d, Y',strtotime($startshift )) . '&nbsp;&nbsp;&nbsp;Shift Hrs: '. date('g:i A',strtotime( $startshift)). ' - ' . date('g:i A',strtotime( $end )),
			'sql'	   => $sql	
			);

		$report = new report($arrReport);
		$report->buildReport();
		return $report->html();
	}
}
