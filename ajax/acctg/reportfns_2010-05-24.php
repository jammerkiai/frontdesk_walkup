<?php

function getHotel() 
{
	return "Shogun 1";
}


function makeShiftSelect($selected=0){
	$retval="<select name='shiftno'>";
	$retval.="<option value='0'>All</option>";
	for($x=1; $x <= 3; $x++) {
		 $retval.="<option value='$x' ";
		 $retval.=($x==$selected) ? " selected " : "";
		 $retval.=">$x</option>";
	}
	$retval.="</select>";
	return $retval;
}

function makeSiteSelect($selected=0){
	$arrSite = array('All','Walkup','Hotel');
	$retval="<select name='siteid'>";
	for($x=0; $x < count($arrSite); $x++) {
		 $retval.="<option value='$x' ";
		 $retval.=($x==$selected) ? " selected " : "";
		 $retval.=">$arrSite[$x]</option>";
	}
	$retval.="</select>";
	return $retval;
}

function getSpecialFloorId()
{
	$sql  ="select settings_value from settings where settings_name='SPECIALFLOORID'";
	$res = mysql_query($sql);
	$row = mysql_fetch_row($res);
	return $row[0];
}

function getSpecialRoomlist($floor)
{
	$sql  ="select room_id from rooms where floor_id='$floor'";
	$res = mysql_query($sql);
	$row = mysql_fetch_row($res);
	$arr = array();
	while($row = mysql_fetch_row($res)) {
		$arr[].=$row[0];
	}
	return implode(',',$arr);
}

function isSpecialFloor($occupancy)
{
	$exceptList  = explode(',',getSpecialRoomList(getSpecialFloorId()));
	$sql  = "select room_id from occupancy where occupancy_id='$occupancy'";
	$res = mysql_query($sql);
	$row = mysql_fetch_row($res);
	return in_array($exceptList,$row[0]) ;
}

