<?php
/**
roomavail.php
*/
include_once("config/config.inc.php");
include_once("date.functions.php");
include_once("reserve.functions.php");


$mo =$_GET["mo"];
$yr = $_GET["yr"];

$date = "$yr-$mo-01";

$sql = 
	" 
	select rooms.room_id, rooms.door_name, room_types.room_type_name, themes.theme_name
	from rooms 
	left join room_types on rooms.room_type_id=room_types.room_type_id
	left join themes on rooms.theme_id=themes.theme_id
	where rooms.site_id=2 ";

$sql.=" and rooms.room_id not in (select room_id from reserve_rooms  where checkin='$date')";

$arr = getdaterange("$yr-$mo-01", 31);
while(count($arr) > 1) {
	list($yyyy,$mmmm,$dddd) = explode("-",$arr[count($arr)-1]);
	if($mmmm!=$mo) array_pop($arr);
	else break;
}	

$monthname = date("F", mktime(0,0,0,$mo,1,$yr));	
$res=mysql_query($sql) or die(mysql_error());
if(mysql_num_rows($res)) 
{
	
    $enddate = $arr[count($arr)-1];
	$ret = "<table id='guestreservetable'>";
	$ret.="<tr><th>&nbsp;</th><th>Room</th><th>Type</th><th>Theme</th><th>$monthname $yr</th></tr>";
	while(list($room,$door,$rtype,$theme)=mysql_fetch_row($res)) {
		$each="";
		foreach($arr as $eachdate){
			list($yy, $mm, $dd) = explode("-",$eachdate);
			$class = (is_reserved($room,$eachdate)) ? "reserved" :"";
			$each .="<li class='$class'>$dd</li>";
		}
		$ret .= "<tr>";
		$ret .= "<td><input type='checkbox' name='addroom[]' id='rm_$room' value='$room'></td>
			<td><label for='rm_$room'> $door</td><td> $rtype</td><td> $theme</td>
			<td><input type='hidden' name='add_checkin_$room' id='add_ci_$room' value='$date' class='roomdate' />
				<ul id='avlist'>$each</ul>
			</td>";
			//<td><input type='text' name='add_checkout_$room' id='add_co_$room' value='$tomorrow' class='roomdate' /></td>
		$ret .= "</tr>";

	}
	$ret .= "</table>";
}
else
{
	$ret="<span style='font-size:.7em;color:#ff0000'>no available rooms</span>";
}
?>
<style>
body {font-family:lucida,arial,helvetica}
a {font-family:lucida,arial,helvetica; text-decoration:none; font-size:.8em;padding:2px;border:1px solid #eeeeee;background-color:#95D5EF}
#guestreservetable {empty-cells:show; border-collapse:false;border-spacing:0px;width:100%;font-family:lucida,arial,helvetica}
#guestreservetable th{font-size:.7em;border-bottom:1px solid #333333;text-align:left}
#guestreservetable td{font-size:.7em;border-bottom:1px dotted #999999;}
#avlist { list-style:none;margin-left:-40px;margin-top:0px;}
#avlist li {float:left;border:1px solid #95D5EF;padding:2px;margin:1px;}
#avlist li.reserved {float:left;border:1px solid #ff6600;background-color:#ffcc99;}
.cmdbtn {
	background-color:#95D5EF;
	border:1px solid #E0EBEF;
	width:80px;
	padding:4px;
	cursor:pointer;
}
</style>
<form>
<a href="reserve.php">Back</a>
<hr />
<div><?=$ret?></div>
</form>


