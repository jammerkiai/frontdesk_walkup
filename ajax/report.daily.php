<?php
session_start();
include_once("config/config.inc.php");
include_once("monthly.class.php");

$lsql = "select settings_value from settings where id = '3'";
$lres = mysql_query($lsql);
list($lobbyid)=mysql_fetch_row($lres);



function getReport($month,$year)
{

	

	$num = cal_days_in_month(CAL_GREGORIAN, $month, $year) ;

	
	$ret = "<table border=1 cellpadding=3 cellspacing=0>";
	$ret .= "<tr>";
	$ret .= "<th>&nbsp;</th>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<th colspan='3' style='text-align:center;border-top:solid thick;border-left:solid thick;'># OF GUEST</th>";
	$ret .= "<th colspan='3' style='text-align:center;border-top:solid thick;border-right:solid thick;'>TOTAL SALES</th>";
	$ret .= "<th colspan='3' style='text-align:center;border-top:solid thick;border-left:solid thick;'>ROOM SALES</th>";
	$ret .= "<th colspan='3' style='text-align:center;border-top:solid thick;'>OVERTIME</th>";
	$ret .= "<th colspan='3' style='text-align:center;border-top:solid thick;'>FOOD</th>";
	$ret .= "<th colspan='3' style='text-align:center;border-top:solid thick;'>BEER</th>";
	$ret .= "<th style='text-align:center;border-top:solid thick;'>Kitchen</th>";
	$ret .= "<th colspan='3' style='text-align:center;border-top:solid thick;border-right:solid thick;'>MISCELLANEOUS</th>";
	$ret .= "</tr>";
	$ret .= "<tr>";
	$ret .= "<td>&nbsp;</td>";	
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td style='border-left:solid thick;'>walkup</td>";
	$ret .= "<td>hotel</td>";
	$ret .= "<td>total</td>";	
	$ret .= "<td>walkup</td>";
	$ret .= "<td>hotel</td>";
	$ret .= "<td style='border-right:solid thick'>total</td>";
	$ret .= "<td style='border-left:solid thick;'>walkup</td>";
	$ret .= "<td>hotel</td>";
	$ret .= "<td>total</td>";
	$ret .= "<td>walkup</td>";
	$ret .= "<td>hotel</td>";
	$ret .= "<td>total</td>";
	$ret .= "<td>walkup</td>";
	$ret .= "<td>hotel</td>";
	$ret .= "<td>total</td>";
	$ret .= "<td>walkup</td>";
	$ret .= "<td>hotel</td>";
	$ret .= "<td>total</td>";
	$ret .= "<td>total</td>";
	$ret .= "<td>walkup</td>";
	$ret .= "<td>hotel</td>";
	$ret .= "<td style='border-right:solid thick;'>total</td>";
	$ret .= "</tr>";
	for($i = 1;  $i <= $num; $i++)
	{

		$start = date('Y-m-d H:i:s', strtotime('-1800 seconds',strtotime($year."-".$month."-".$i." 00:00:00")));
		$end = date('Y-m-d H:i:s', strtotime('+1800 seconds',strtotime($year."-".$month."-".$i." 23:59:59")));

		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift ='start' order by `datetime` asc limit 0,1";
		$res = mysql_query($sql);
		list($startdt)=mysql_fetch_row($res);
		
		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift ='end' order by `datetime` desc limit 0,1";
		$res = mysql_query($sql);
		list($enddt)=mysql_fetch_row($res);
		
		$walkup = new monthly(1,$startdt,$enddt);
		$hotel = new monthly(2,$startdt,$enddt);
        $all =  new monthly('',$startdt,$enddt);


		$ret .= "<tr>";
		$ret .= "<td>$i</td>";
		$ret .= "<td>$i</td>";
		$ret .= "<td style='border-left:solid thick;text-align:right'>".number_format($walkup->numguest)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($hotel->numguest)."</td>";
		$ret .= "<td style='text-align:right'><b>".number_format($all->numguest)."</b></td>";

		$ret .= "<td style='text-align:right'>".number_format($walkup->totalsales)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($hotel->totalsales)."</td>";
		$ret .= "<td style='border-right:solid thick;text-align:right''><b>".number_format($all->totalsales)."</b></td>";

		$ret .= "<td style='border-left:solid thick;text-align:right''>".number_format($walkup->roomsales)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($hotel->roomsales)."</td>";
		$ret .= "<td style='text-align:right'><b>".number_format($all->roomsales)."</b></td>";
		
		$ret .= "<td style='text-align:right'>".number_format($walkup->overtimesales)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($hotel->overtimesales)."</td>";
		$ret .= "<td style='text-align:right'><b>".number_format($all->overtimesales)."</b></td>";

		$ret .= "<td style='text-align:right'>".number_format($walkup->foodsales)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($hotel->foodsales)."</td>";
		$ret .= "<td style='text-align:right'><b>".number_format($all->foodsales)."</b></td>";

		$ret .= "<td style='text-align:right'>".number_format($walkup->beersales)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($hotel->beersales)."</td>";
		$ret .= "<td style='text-align:right'><b>".number_format($all->beersales)."</b></td>";

		$ret .= "<td style='text-align:right'><b>".number_format($all->kitchen)."</b></td>";

		$ret .= "<td style='text-align:right'>".number_format($walkup->miscsales)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($hotel->miscsales)."</td>";
		$ret .= "<td style='border-right:solid thick;text-align:right'><b>".number_format($all->miscsales)."</b></td>";

		$ret .= "</tr>";

		$walkupNumGuestTotal += $walkup->numguest;
		$hotelNumGuestTotal += $hotel->numguest;
		$allNumGuestTotal += $all->numguest;

		$walkupTotalSales += $walkup->totalsales;
		$hotelTotalSales += $hotel->totalsales;
		$allTotalSales += $all->totalsales;

		$walkupRoomSales += $walkup->roomsales;
		$hotelRoomSales += $hotel->roomsales;
		$allRoomSales += $all->roomsales;

		$walkupOvertimeSales += $walkup->overtimesales;
		$hotelOvertimeSales += $hotel->overtimesales;
		$allOvertimeSales += $all->overtimesales;

		$walkupFoodSales += $walkup->foodsales;
		$hotelFoodSales += $hotel->foodsales;
		$allFoodSales += $all->foodsales;

		$walkupBeerSales += $walkup->beersales;
		$hotelBeerSales += $hotel->beersales;
		$allBeerSales += $all->beersales;

		$allKitchenSales += $all->kitchen;

		$walkupMiscSales += $walkup->miscsales;
		$hotelMiscSales += $hotel->miscsales;
		$allMiscSales += $all->miscsales;

	}
	$ret .= "<tr>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td style='border-left:solid thick;border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($walkupNumGuestTotal)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($hotelNumGuestTotal)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($allNumGuestTotal)."</td>";

	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($walkupTotalSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($hotelTotalSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;border-right:solid thick;font-weight: bold;text-align:right'>".number_format($allTotalSales)."</td>";

	$ret .= "<td style='border-bottom:solid thick;border-left:solid thick;font-weight: bold;text-align:right'>".number_format($walkupRoomSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($hotelRoomSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($allRoomSales)."</td>";
		
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($walkupOvertimeSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($hotelOvertimeSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($allFoodSales)."</td>";

	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($walkupFoodSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($hotelFoodSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($allFoodSales)."</td>";

	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($walkupBeerSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($hotelBeerSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($allBeerSales)."</td>";

	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($allKitchenSales)."</td>";

	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($walkupMiscSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;font-weight: bold;text-align:right'>".number_format($hotelMiscSales)."</td>";
	$ret .= "<td style='border-bottom:solid thick;border-right:solid thick;font-weight: bold;text-align:right'>".number_format($allMiscSales)."</td>";
	$ret .= "</tr>";
	$ret .= "</table>";

	return $ret;
}


	function getMonthDropdown($name="month", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => 'January',
					2 => 'February',
					3 => 'March',
					4 => 'April',
					5 => 'May',
					6 => 'June',
					7 => 'July',
					8 => 'August',
					9 => 'September',
					10 => 'October',
					11 => 'November',
					12 => 'December');
			/*** the current month ***/
			$selected = is_null($selected) ? date('n', time()) : $selected;

			for ($i = 1; $i <= 12; $i++)
			{
					$dd .= '<option value="'.$i.'"';
					if ($i == $selected)
					{
							$dd .= ' selected';
					}
					/*** get the month ***/
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}

	function getYearDropdown($name="year", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => '2009',
					2 => '2010',
					3 => '2011',
					4 => '2012',
					5 => '2013',
					6 => '2014',
					7 => '2015',
					8 => '2016');
		   
			$selected = is_null($selected) ? date('n', time()) : $selected;

			for ($i = 1; $i <= 8; $i++)
			{
					$dd .= '<option value="'.$months[$i].'"';
					if ($months[$i] == $selected)
					{
							$dd .= ' selected';
					}
					
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}
?>
<style>
		.printable {
			font-family: sans-serif;
			font-size: 14px;
			font-weight: 550;
			}
		.report{
			font-family: sans-serif;
			font-size: 14px ;
			text-align:left;
			font-weight: 550;
				
		}
		.report td{
			padding-bottom:12px;
		}
		.summary{
			font-family: sans-serif;
			font-size: 14px;
			text-align:left;
			font-weight: 550;
		}
</style>
<script src="../js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.print.js"></script>
<script type="text/javascript">
 
		// When the document is ready, initialize the link so
		// that when it is clicked, the printable area of the
		// page will print.
		$(document).ready(function(){		
				$("a").attr( "href", "javascript:void( 0 )" ).click(
						function(){
							// Print the DIV.
							$(".printable").print(); 
							// Cancel click event.
							return( false );
						});
 
			
		});
 
</script>
<form name=myform method=post>
<div>
Month: <? echo getMonthDropdown("ddlmonth",$_POST["ddlmonth"]); ?>
<br>
<br>
Year: <? echo getYearDropdown("ddlyear",$_POST["ddlyear"]); ?>
</div>
<br>
<input type='submit' value='Search' name='btnSearch' />
<br>
<br>
<a href="#">Print Report</a>
<br>
<br>
<div class='printable'>
<? if($_POST){ echo getReport($_POST["ddlmonth"],$_POST["ddlyear"]);} ?>
</div><br />
<a href="#">Print Report</a>
</form>