<?php
session_start();
include_once("config/config.inc.php");
require_once('acctg/class.baseobject.php');
require_once('acctg/class.shift.php');
require_once('acctg/class.report.php');
require_once('acctg/reportfns.php');

$occ = $_GET["occ"];

if(isset($_POST) && $_POST['submit']=='Save') {
	$date = getAdjustDate($occ);
	$updateby='20';
	$remarks = $_POST['newremarks'];
	$amount = $_POST['newamount'];
	switch($_POST['newsalestype']) {
		case 'room':
			$table='room_sales';
			$category=3;
			$item=18;
			break;
		case 'misc':
			$table='room_sales';
			$category=1;
			$item=29;
			break;
		case 'food':
			$table='fnb_sales';
			$category=22;
			$item=114;
			break;
		case 'beer':
			$table='fnb_sales';
			$category=21;
			$item=113;
			break;
	}
	if($table!='' && $category!='' && $item!='' && $amount!='' && $remarks!='') {
		$sql = "insert into $table (sales_date,update_date,occupancy_id,category_id,item_id,unit_cost,qty,status,remarks,update_by) 
		values ('$date','$date',$occ,$category,$item,'$amount',1,'Paid','$remarks','$updateby') ";
		mysql_query($sql);
		//echo $sql;
		header('location: occupancydetails.php?occ='.$occ);
	}else{
		$error = "All data is required.";
	}
}

function getAdjustDate($occ) {
	$sql = "select actual_checkout from occupancy where occupancy_id='$occ'";
	$res = mysql_query($sql) or die($sql);
	$row = mysql_fetch_row($res);
	if($row[0]!='0000-00-00 00:00:00') {
		return date('Y-m-d H:i:s', strtotime($row[0] . '-1 minute'));
	}else{
		return date('Y-m-d H:i:s');
	}
}

?>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<form method='post' action=''>
<div class='toolbar'>
<span style='padding-right:4px;font-weight:bold;color:#996633;'>Apply Adjustment: </span>
<select name='newsalestype' >
<option value=''></option>
<option value='room'>Room</option>
<option value='misc'>Misc</option>
<option value='food'>Fnb</option>
<option value='beer'>beer</option>
</select>
<label for='adjvalue'>Amount</label> <input type='text' id='newamount' name='newamount' />
<label for='adjvalue'>Remarks</label> <input type='text' id='newremarks' name='newremarks' />
<input type='submit' name='submit' value='Save' />
<input type='submit' name='submit' value='Refresh' />
<input type='hidden' name='occ' id='occ' value='$occ' />
</div>
</form>
<?php

echo $error;

$totals=array();
$sql  ="select a.actual_checkin, a.expected_checkout, a.actual_checkout,
		a.shift_checkin, b.door_name, c.rate_name, d.fullname
		from occupancy a, rooms b, rates c, users d 
		where 
		a.room_id=b.room_id
		and a.rate_id=c.rate_id
		and a.update_by=d.user_id
		and a.occupancy_id=$occ
		";

$arrReport = array(
		'title'    =>  "Guest Occupancy Details: $occ",
		'aggregates'=> array('total_cost'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();


$sql  ="select a.roomsales_id, a.sales_date, a.order_code, 
		c.sas_cat_name, b.sas_description, a.unit_cost,
		a.qty, (a.unit_cost* a.qty) as total, a.status, a.remarks, a.update_date, d.fullname 
		from room_sales a, sales_and_services b, sas_category c ,users d
		where 
		a.item_id=b.sas_id
		and a.category_id=c.sas_cat_id
		and a.category_id=3
		and a.update_by=d.user_id
		and a.occupancy_id=$occ";
$arrReport = array(
		'title'    =>  "Room Charges Details: $occ",
		'aggregates'=> array('total'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();
$totals['room']=$report->aggregate['total'];

$sql  ="select a.roomsales_id, a.sales_date, a.order_code, 
		c.sas_cat_name, b.sas_description, a.unit_cost,
		a.qty, (a.unit_cost* a.qty) as total, a.status, a.remarks, a.update_date, d.fullname
		from room_sales a, sales_and_services b, sas_category c ,users d
		where 
		a.item_id=b.sas_id
		and a.category_id=c.sas_cat_id
		and a.category_id not in (2,3)
		and a.update_by=d.user_id
		and a.occupancy_id=$occ";
$arrReport = array(
		'title'    =>  "Misc Sales Details: $occ",
		'aggregates'=> array('total'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();
$totals['misc']=$report->aggregate['total'];

$sql  ="select a.roomsales_id, a.sales_date, a.order_code, 
		c.sas_cat_name, b.sas_description, a.unit_cost,
		a.qty, (a.unit_cost* a.qty) as total, a.status, a.remarks, a.update_date, d.fullname
		from room_sales a, sales_and_services b, sas_category c , users d
		where 
		a.item_id=b.sas_id
		and a.category_id=c.sas_cat_id
		and a.category_id=2
		and a.update_by=d.user_id
		and a.occupancy_id=$occ";
$arrReport = array(
		'title'    =>  "Coop Sales Details: $occ",
		'aggregates'=> array('total'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();
$totals['coop']=$report->aggregate['total'];


$sql  ="select a.fnbsales_id, a.sales_date, a.order_code, 
		c.food_category_name, b.fnb_name, a.unit_cost,
		a.qty, (a.unit_cost* a.qty) as total, a.status, a.remarks, a.update_date, d.fullname
		from fnb_sales a, fnb b, food_categories c, users d  
		where 
		a.category_id=c.food_category_id
		and a.item_id=b.fnb_id
		and a.category_id not in (17,21)
		and a.update_by=d.user_id
		and a.occupancy_id=$occ";
$arrReport = array(
		'title'    =>  "Food Sales Details: $occ",
		'aggregates'=> array('total'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();
$totals['food']=$report->aggregate['total'];

$sql  ="select a.fnbsales_id, a.sales_date, a.order_code, 
		c.food_category_name, b.fnb_name, a.unit_cost,
		a.qty, (a.unit_cost* a.qty) as total, a.status, a.remarks, a.update_date, d.fullname
		from fnb_sales a, fnb b, food_categories c, users d  
		where 
		a.category_id=c.food_category_id
		and a.item_id=b.fnb_id
		and a.category_id in (17,21)
		and a.update_by=d.user_id
		and a.occupancy_id=$occ";
$arrReport = array(
		'title'    =>  "Beer Sales Details: $occ",
		'aggregates'=> array('total'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();
$totals['beer']=$report->aggregate['total'];

$sql  ="select a.receipt_date, a.tendertype, a.amount, b.fullname  
		from salesreceipts a, users b 
		where a.update_by=b.user_id
		and a.occupancy_id=$occ";
$arrReport = array(
		'title'    =>  "Payment Details: $occ",
		'aggregates'=> array('amount'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();
$totals['receipts']=$report->aggregate['amount'];



?>
<h4>Summary of sales and payments</h4>
<table class='summary'>
<tr>
<th>&nbsp;</th>
<th width='100'>Sales</th>
<th width='100'>Payments</th>
</tr>
<tr>
<th>Room</th>
<td><?php echo $totals['room'] ?></td>
<td>&nbsp;</td>
</tr>
<tr>
<th>Misc</th>
<td>&nbsp;<?php echo $totals['misc'] ?></td>
<td>&nbsp;</td>
</tr>
<tr>
<th>Coop</th>
<td>&nbsp;<?php echo $totals['coop'] ?></td>
<td>&nbsp;</td>
</tr>
<tr>
<th>Food</th>
<td>&nbsp;<?php echo $totals['food'] ?></td>
<td>&nbsp;</td>
</tr>
<tr>
<th>Beer</th>
<td>&nbsp;<?php echo $totals['beer'] ?></td>
<td>&nbsp;</td>
</tr>
<tr>
<th>Payments</th>
<td>&nbsp;</td>
<td><?php echo $totals['receipts'] ?></td>
</tr>
<tr>
<th>Totals:</th>
<th class='aggregates'><?php echo $totals['room'] + $totals['misc'] +
	$totals['coop'] + $totals['food'] + $totals['beer']
?></th>
<th class='aggregates'><?php echo $totals['receipts'] ?></th>
</tr>

</table>
<style>
body,h1,h2,h3,h4,h5 {
	font-family: arial, helvetica, sans-serif;
	margin:0;
	margin-width:0;
	margin-height:0;
	font-size:14px;
}

.toolbar {
			background-color:#cccccc;
			padding:4px;
		}

div {
	font-size:13px;
}

h1 {
	font-size:14px;
	padding-top:8px;
	border-top:1px solid #111199;
	
}

table {
	font-size:12px;
	padding:2px;
	border:1px solid #dddddd;
}

th, td{
	background-color:#eeeeee;
	padding:2px;
}
.summary th, td {
	text-align:right;
	border-bottom:1px dotted #cccccc;
}
td {
	background-color:#fcfcfc;
}

.aggregates th{
	border-top:1px solid #000000;
}
</style>

