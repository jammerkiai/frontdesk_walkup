<?php
/**
* reserve.functions.php
*/

function getreservedbyguest($gid=0,$fn='',$mn='',$ln=''){
	$sql = " select a.reserve_code, a.reserve_date, a.reserve_fee, a.pax, a.status, b.firstname,  b.middlename, b.lastname
			from reservations a, guests b where a.guest_id=b.guest_id ";
	if($gid) $sql.=" and b.guest_id='$gid' ";
	
	
	if($fn) $sql.=" and b.firstname = '$fn' ";
	if($mn) $sql.=" and b.middlename = '$mn' ";
	if($ln) $sql.=" and b.lastname = '$ln' ";
	
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res)) {
		$ret = "<table id='guestreservetable'>";
		$ret.="<tr><th>RC#</th><th>Date</th><th>Guest Name</th><th>Fee</th><th>Pax</th><th>Status</th></tr>";
		while(list($code,$date,$fee,$pax,$stat,$f,$m,$l)=mysql_fetch_row($res)) {
			$ret.="<tr>";
			$ret.="<td><a href='reserve.php?code=$code'>$code</a></td>";
			$ret.="<td>$date</td>";
			$ret.="<td>$f $m $l</td>";
			$ret.="<td>$fee</td>";
			$ret.="<td>$pax</td>";
			$ret.="<td>$status</td>";
			$ret.="</tr>";
		}
		$ret .= "</table>";
	}else{
		$ret = "<span >no reservations found</span>";
	}
	return $ret ;
}

function getall() {
	$sql = "select rooms.room_id, rooms.door_name, room_types.room_type_name, themes.theme_name
		from rooms left join room_types on rooms.room_type_id=room_types.room_type_id
		left join themes on rooms.theme_id=themes.theme_id
		where rooms.site_id=2";
	$res = mysql_query($sql) or die(mysql_error());
	while(list($rid,$door,$type,$theme)=mysql_fetch_row($res)) {
		$retval.="<tr>";
		$retval.="<td>$rid</td>";
		$retval.="<td>$door</td>";
		$retval.="<td>$type</td>";
		$retval.="<td>$theme</td>";
		$retval.="</tr>";
	}
	return $retval;
}



function getroomtypes($selected) {
	$sql = "select distinct room_type_id, room_type_name from room_types order by room_type_name ";
	$sql = "select distinct room_types.room_type_id, room_types.room_type_name
		from room_types  left join rooms on room_types.room_type_id=rooms.room_type_id
		where rooms.site_id=2";
	
	$res = mysql_query($sql) or die(mysql_error());
	while(list($id, $name)=mysql_fetch_row($res)) {
		$retval.="<option value='$id'";
		$retval.= ($id==$selected) ? " selected " : "";
		$retval.=">$name</option>";
	}
	return $retval;
}


function getthemes($selected) {
	$sql = "select theme_id, theme_name from themes order by theme_name ";
	$res = mysql_query($sql) or die(mysql_error());
	while(list($id, $name)=mysql_fetch_row($res)) {
		$retval.="<option value='$id'";
		$retval.= ($id==$selected) ? " selected " : "";
		$retval.=">$name</option>";
	}
	return $retval;
}

function getrooms($roomtype, $theme=0) {	
	$sql = "select distinct rooms.room_id, rooms.door_name, themes.theme_name
		from rooms left join themes on rooms.theme_id=themes.theme_id
		where   rooms.site_id=2";
	if($roomtype) $sql.=" and rooms.room_type_id='$roomtype'";	
	elseif($theme) $sql.=" and rooms.theme_id='$theme'";	
	
	$res = mysql_query($sql) or die(mysql_error());
	while(list($id, $name, $theme)=mysql_fetch_row($res)) {
		$retval.="<option value='$id'";
		$retval.= ($id==$selected) ? " selected " : "";
		$retval.=">$name </option>";
	}
	return $retval;
}

function getnextreservecode($pre=1) {
	return $pre . "-" . mktime();
}


function getreservedroomsbycode($code) {
	$sql = 
		" 
		select rooms.room_id, rooms.door_name, room_types.room_type_name, themes.theme_name,reserve_rooms.reserve_code, reserve_rooms.checkin, reserve_rooms.checkout,reserve_rooms.rr_id
		from rooms 
		left join reserve_rooms on rooms.room_id=reserve_rooms.room_id 
		left join room_types on rooms.room_type_id=room_types.room_type_id
		left join themes on rooms.theme_id=themes.theme_id
		where rooms.site_id=2 and reserve_rooms.reserve_code='$code'
		";
		$res=mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res)) {	
		$ret = "
		<input type='button' class='cmdbtn' id='delroom' value='Delete' />
		<table id='guestreservetable'>";
		$ret.="<tr><th>&nbsp;</th><th>Room</th><th>Type</th><th>Theme</th><th>Day Reserved</th></tr>";
		while(list($room,$door,$rtype,$theme,$code,$in,$out,$rrid)=mysql_fetch_row($res)) {
			$ret .= "<tr>";
			$ret .= "<td><input type='checkbox' name='editroom[]' id='rm_$room' value='$rrid'></td>
				<td><label for='rm_$room'> $door</td><td> $rtype</td><td> $theme</td>
				<td><input type='hidden' name='edit_checkin_$rrid' id='edit_ci_$room' value='$in' class='roomdate' />$in</td>";
				//<td><input type='text' name='edit_checkout_$rrid' id='edit_co_$room' value='$out' class='roomdate' /></td>
			$ret .= "</tr>";
		}
		$ret .= "</table>";
	}else{
		$ret = "<span style='font-size:.7em;color:#ff0000'>no rooms found</span>";
	}
	return $ret;
}


function getreservedroomsbydate($date) {
		$sql = 
		" 
		select rooms.room_id, rooms.door_name, room_types.room_type_name, themes.theme_name,reserve_rooms.reserve_code, reserve_rooms.checkin, reserve_rooms.checkout
		from rooms 
		left join room_types on rooms.room_type_id=room_types.room_type_id
		left join themes on rooms.theme_id=themes.theme_id
		left join reserve_rooms on rooms.room_id=reserve_rooms.room_id
		where rooms.site_id=2 ";
		$sql.=" and rooms.room_id in (select room_id from reserve_rooms  where checkin='$date' )";
	
	
	$res=mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res)) {
	$ret = "<table id='guestreservetable'>";
	$ret.="<tr><th>&nbsp;</th><th>Room</th><th>Type</th><th>Theme</th><th>Checkin</th><th>Checkout</th><th>Reservation Code</th></tr>";
	while(list($room,$door,$rtype,$theme,$code,$in,$out)=mysql_fetch_row($res)) {
		$ret .= "<tr>";
		$ret .= "<td><input type='checkbox' name='addroom[]' id='rm_$room' value='$room'></td>
			<td><label for='rm_$room'> $door</td><td> $rtype</td><td> $theme</td><td>$in</td><td>$out</td><td><a href='reserve.php?code=$code'>$code</a></td>";
		$ret .= "</tr>";
	}
	$ret .= "</table>";
	}else{
		$ret = "<span style='font-size:.7em;color:#ff0000'>no reservations found</span>";
	}
	return $ret;
}

function is_reserved($room,$date) {
	$sql ="select rr_id from reserve_rooms where checkin='$date' and room_id='$room' ";
	$res = mysql_query($sql) or die(mysql_error());
	if( mysql_num_rows($res)) return true;
	else return false;
}


/**
* $filter = actual roomtype or theme_id passed
* $type= none='', roomtype=1, theme=2
*/
function getavailable($date, $filter='', $type='') {
	$sql = 
	" 
	select rooms.room_id, rooms.door_name, room_types.room_type_name, themes.theme_name
	from rooms 
	left join room_types on rooms.room_type_id=room_types.room_type_id
	left join themes on rooms.theme_id=themes.theme_id
	where rooms.site_id=2 ";
	if($type==1) {
		if($filter) $sql.=" and room_types.room_type_id='$filter' "; 
	}elseif($type==2) {
		if($filter) $sql.=" and themes.theme_id='$filter' ";
	}
	$sql.=" and rooms.room_id not in (select room_id from reserve_rooms  where checkin='$date')";
	
	
	$res=mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res)) {
	$arr = getdaterange($date,7);
	
	$enddate = $arr[count($arr)-1];
	$ret = "<table id='guestreservetable'>";
	$ret.="<tr><th>&nbsp;</th><th>Room</th><th>Type</th><th>Theme</th><th>7-day Schedule ($date  to  $enddate)</th></tr>";
	while(list($room,$door,$rtype,$theme)=mysql_fetch_row($res)) {
		$each="";
		foreach($arr as $eachdate){
			list($yy, $mm, $dd) = explode("-",$eachdate);
			$class = (is_reserved($room,$eachdate)) ? "reserved" :"";
			$each .="<li class='$class'>$dd</li>";
		}
		$ret .= "<tr>";
		$ret .= "<td><input type='checkbox' name='addroom[]' id='rm_$room' value='$room'></td>
			<td><label for='rm_$room'> $door</td><td> $rtype</td><td> $theme</td>
			<td><input type='hidden' name='add_checkin_$room' id='add_ci_$room' value='$date' class='roomdate' />
				<ul id='avlist'>$each</ul>
			</td>";
			//<td><input type='text' name='add_checkout_$room' id='add_co_$room' value='$tomorrow' class='roomdate' /></td>
		$ret .= "</tr>";

	}
	$ret .= "</table>";
	}else{
		$ret="<span style='font-size:.7em;color:#ff0000'>no available rooms</span>";
	}
	return $ret;
}
?>