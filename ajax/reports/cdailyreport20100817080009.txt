MSHOGUN 1: SHIFT END REPORT
Tuesday August 17, 2010 8:00:05 AM

SALES TRANSACTION
Room Sales Order			21190
FnB Sales Order			1207
Beer Sales Order			290
Misc Sales Order			60
Room Sales Adjustment			555
GROSS SALES			23302

DISCOUNT
Room				0
FnB				0
Misc				0

DEPOSIT				2148
REFUND				

TOTAL ACCOUNTABILITY			25450

CASH DECLARATION			8384
CARD				595
FUNDS SAFEKEEP			0
TOTAL CASH DECLARED			8979

TOTAL CASH ON SYSTEM			8384
OVER/SHORTAGES 			0

ROOM	 # OF CHECK-OUT	AMT
Value 		6	3330
Regular 		9	5205
Classique 		16	8860
Premium 		1	750
Regent 		3	2400
Queen 		0	0
Gemini 		0	0
Family 		0	0
Executive Suite 	0	0
Mega Suite 	1	1200

DISCOUNT 
Value 			  
Regular 		  
Classique 		  
Premium 		  
Regent 			  
Queen 		  
Gemini 		  
Family 		  
Executive Suite 	  
Mega Suite 		  


=== RESERVATIONS ===

Deposits:


Claimed:



SECURITY DEPOSITS

Deposits:
Claimed:


=== SALES BREAKDOWN ===

Transaction Date	Room No.	Cash	Card

2010-08-17 00:08:58 	306 		395 	0 	
2010-08-17 00:09:56 	218 		100 	0 	
2010-08-17 00:13:19 	204 		132 	0 	
2010-08-17 00:13:29 	510 		350 	0 	
2010-08-17 00:13:39 	220 		415 	0 	
2010-08-17 00:17:40 	210 		636 	0 	
2010-08-17 00:32:27 	206 		345 	0 	
2010-08-17 00:41:21 	315 		90 	0 	
2010-08-17 01:11:11 	522 		750 	0 	
2010-08-17 01:58:36 	303 		705 	0 	
2010-08-17 02:04:09 	314 		345 	0 	
2010-08-17 02:05:58 	316 		10 	0 	
2010-08-17 02:09:53 	221 		395 	0 	
2010-08-17 03:00:33 	212 		300 	0 	
2010-08-17 03:26:27 	311 		100 	0 	
2010-08-17 04:27:20 	316 		10 	0 	
2010-08-17 04:27:43 	424 		55 	0 	
2010-08-17 04:28:17 	301 		316 	0 	
2010-08-17 04:59:43 	309 		395 	0 	
2010-08-17 05:04:48 	319 		395 	0 	
2010-08-17 05:12:50 	303 		20 	0 	
2010-08-17 05:18:39 	403 		345 	0 	
2010-08-17 05:25:45 	517 		50 	0 	
2010-08-17 05:27:29 	322 		316 	0 	
2010-08-17 06:26:25 	507 		260 	0 	
2010-08-17 06:49:16 	506 		60 	0 	
2010-08-17 07:22:24 	312 		316 	0 	
2010-08-17 07:29:58 	324 		20 	0 	
2010-08-17 07:33:43 	520 		363 	0 	
2010-08-17 07:35:19 	407 		140 	0 	
2010-08-17 07:49:18 	513 		25 	0 	
2010-08-17 07:49:19 	513 		200 	0 	
2010-08-17 07:49:21 	513 		30 	0 	
 			Total:		8384 	0		

=== RECHIT LIST ===




V1