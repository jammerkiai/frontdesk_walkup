<?php
/**
* reserve.php 
*
* process:
	1. ask for guest name
		--> check system if guest information exists
	2. ask for number of guests
	3. ask for inclusive dates of stay
	4. ask for preference
*
* views:
* 	1. new reservation:
		- select hotel rooms
		- 
	2. view reservations:
		- from dropdown of rooms
*/

include_once("config/config.inc.php");
include_once("date.functions.php");
include_once("reserve.functions.php");

foreach($_POST as $key=>$value) $$key=$value;

$in=date("Y-m-d");
$out = my_date_add($in,1,'D');

if(isset($_GET["code"])) {
	$thiscode = $_GET["code"];
	$sql  = "select a.guest_id, a.pax, a.reserve_fee, a.reserve_date, b.firstname,b.middlename,b.lastname, a.notes
		from reservations a, guests b 
		where a.guest_id=b.guest_id and a.reserve_code='$thiscode' ";
	$res = mysql_query($sql) or die(mysql_error());
	list($guest_id, $pax, $reserve_fee, $reserve_date, $firstname, $middlename, $lastname,$notes)=mysql_fetch_row($res);
}

if($act=="rtselect") {
	echo getrooms($rtid);
	exit;
}elseif($act=="thselect") {
	echo getrooms(0,$thid);
	exit;
}elseif($act=="guestadd") {
	$fn=$_POST["fn"];
	$mn=$_POST["mn"];
	$ln=$_POST["ln"];
	$sql="insert into guests(firstname, middlename,lastname) values ('$fn','$mn','$ln') ";
	mysql_query($sql) or die(mysql_error());
	echo mysql_insert_id();
	exit;
}elseif($act=="roomlist") {
	if($status=="Available") echo getavailable($wcdate);
	elseif($status=='Reserved') echo getreservedroomsbydate($wcdate);
	else {
		if($theme) {
			echo getavailable($wcdate,$theme,2);
		}elseif($rtype){
			echo getavailable($wcdate,$rtype,1);
		}else{
			echo getreservedroomsbydate($wcdate);
		}
	}
	exit;
}elseif($act=="guestreserve") {
	echo getreservedbyguest($gid,$fn,$mn,$ln);
	exit;
}elseif($act=="delroom") {
	if(is_array($editroom)) {
		$rrlist  = implode(",",$editroom);
		$sql = "delete from reserve_rooms where rr_id in('$rrlist')";
		mysql_query($sql) or die(mysql_error());
	}
	
}elseif($act=="newreserve"){
	$newcode = (isset($new_reserve_code)&&trim($new_reserve_code)!="") ? $new_reserve_code : getnextreservecode();
	if(trim($new_reserve_code)!="") //update
	{
		$sql = "update reservations set 
			reserve_fee='$new_reserve_fee',
			pax='$new_pax',
			notes='$new_notes'
			where reserve_code='$newcode'";
		mysql_query($sql) or die(mysql_error());
		
		if(is_array($addroom)) {
			$values="";
			foreach($addroom as $key=>$roomid) {
				$checkin = ${"add_checkin_$roomid"};
				
				$values .= ($values=="") ? "" : ",";
				$values .= "('$newcode','$roomid','$checkin')";
			}
			$sql = " insert into reserve_rooms (reserve_code, room_id, checkin ) values $values ";
			mysql_query($sql) or die(mysql_error());
		}
		if(is_array($editroom)) {
			foreach($editroom as $key=>$rrid) {
				$checkin = ${"edit_checkin_$rrid"};
				
				$sql = "update reserve_rooms set checkin='$checkin'
					where reserve_code='$newcode' and rr_id='$rrid'
					";
				mysql_query($sql) or die(mysql_error());				
			}
		}
	}
	else //insert
	{
		$sql  =" insert into reservations (reserve_code, guest_id, reserve_date, pax, reserve_fee,notes) 
		values ('$newcode', '$new_guestid', '$new_reserve_date','$new_pax','$new_reserve_fee','$new_notes')
		" ;
		mysql_query($sql) or die(mysql_error());
		if(is_array($addroom)) {
			$values="";
			foreach($addroom as $key=>$roomid) {
				$checkin = ${"add_checkin_$roomid"};
				$checkout = ${"add_checkout_$roomid"};
				$values .= ($values=="") ? "" : ",";
				$values .= "('$newcode','$roomid','$checkin','$checkout')";
			}
			$sql = " insert into reserve_rooms (reserve_code, room_id, checkin, checkout ) values $values ";
			mysql_query($sql) or die(mysql_error());
		}
	}
	
	header("location: reserve.php?code=$newcode");
}



?>
<form method="post" action="" name="reserveform" id="reserveform">
<table id="" width="1000" height="100%" border="0"> 
<tr>
<td valign="top" width="25%">
<fieldset>
<legend>Search Reservations</legend>

<div id='mycal'></div>

<table id="guestinfo" width="100%">
<tbody>
<tr>
<td>
<input type='button' value='Today' id="viewtoday" class="keyselect" style='background-color:#95D5EF;cursor:pointer' />
</td>
</tr>
<tr>
<td>
<input type='button' value='Reservations for the Month' id="viewmonth" class="keyselect" style='background-color:#95D5EF;cursor:pointer' />
</td>
</tr>
<tr>
<td><label for="new_status">By Date</label><br />
<input type='text' id="selecteddate" class="keyselect" />
</td>
</tr>
<tr>
<td><label for="new_status">By Status</label><br />
<select id="new_status" name="new_status" class="keyselect">
<option value="0"></option>
<option value="Available">Available</option>
<option value="Reserved">Reserved</option>
</select>
</td>
</tr>
<tr>
<td><label for="new_roomtype">By Room Type</label><br />
<select id="new_roomtype" name="new_roomtype" class="keyselect">
<option value="0"></option>
<?= getroomtypes() ?>
</select>
</td>
</tr><tr>
<td><label for="new_theme">By Theme</label><br />
<select id="new_theme" name="new_theme" class="keyselect">
<option value="0"></option>
<?= getthemes() ?>
</select>
</td>
</tr><tr>
<td><label for="new_roomid">Room Name</label><br />
<select id="new_roomid" name="new_roomid" class="keyselect">
<option></option>
</select>
</td>
</tr>
<tr>

<td>

<input type="button" value="Search" class="cmdbtn" id="verifyroom" />
</td>
</tr>

</table>
</fieldset>


</td>
<td valign="top">

<fieldset>
<legend>Guest Details</legend>
<table id="guestinfo">
<tr>
<td colspan=3>
<label for="searchkeyword">
<a href="#" onclick="$('#searchcontainer').toggle();return false;">Search Guest Database</a>
<a href="#" id='guestreserve'>Guest Reservations</a>
</label><br />
<div id="searchcontainer" style="display:none">
<input type="text" class="keypadfield" name="searchkeyword" id="searchkeyword"  style='width:80%' />
<input type="button"  name="searchgo" id="searchgo" value="go" class="cmdbtn" style='width:40px;padding:1px;' />
<div id="searchcount"></div>
<table id="searchtable" width="100%">
<thead></thead>
<tbody></tbody> 
</table>
</div>
</td></tr>
<tr>
<td><label for="new_firstname">First Name</label><br />
<input type="text"  name="new_firstname" id="new_firstname" class="keypadfield" value="<?=$firstname?>" /></td>
<td><label for="new_middlename">Middle Name</label><br />
<input type="text" name="new_middlename" id="new_middlename"  class="keypadfield" value="<?=$middlename?>" /></td>
<td><label for="new_lastname">Last Name</label><br />
<input type="text" name="new_lastname" id="new_lastname"  class="keypadfield" value="<?=$lastname?>" />

</td>
<td><input type="button" name="cmdAdd" id="cmdAdd" value="Add" /> </td>
</tr>
</table>

</fieldset>
<?
$now = date("Y-m-d");
if(isset($_GET["view"]) && $_GET["view"]==1) {
	$list =  getreservedroomsbydate($now);
	$legend = "Today's Reservations: $now";
}elseif(isset($_GET["code"]) && $_GET["code"]!=""){	
	$list = getreservedroomsbycode($thiscode);
	$legend = "RC# $thiscode Reserved Rooms";
}else{
	$list = getavailable(date("Y-m-d")); 
	$legend = "Available Rooms Today: $now";
}
?>
<fieldset><legend id='rllegend'><?=$legend?></legend>
<div id="roomlisting">
<?=$list?>
</div>

</fieldset>

</td>

<td valign='top'>
<fieldset><legend>Reservation Details</legend>
<table id="guestinfo">
<tr>
<td><label for="new_reserve_code">Reservation Code</label><br />
<input type="text"  name="new_reserve_code" id="new_reserve_code" value="<?=$thiscode?>" class="numkeypadfield" />
<input type="button" name="findcode" id="findcode" value="find" />
</td>
</tr>
<tr>
<td><label for="new_reserve_date">Reservation Date</label><br />
<input type="text"  name="new_reserve_date" id="new_reserve_date" value="<?=$reserve_date ? $reserve_date : $in?>" class="roomdate" /></td>
</tr>
<tr>
<td><label for="new_reserve_fee">Reservation Fee</label><br />
<input type="text" name="new_reserve_fee" id="new_reserve_fee" value="<?=$reserve_fee ? $reserve_fee : 0?>" class="numkeypadfield" /></td>
</tr><tr>
<td><label for="new_pax">No. of Pax</label><br />
<input type="text" name="new_pax" id="new_pax" value="<?=$pax?$pax:1?>" class="numkeypadfield" value='1' />
</td>
</tr>
<tr>
<td><label for="new_pax">Notes</label><br />
<textarea cols='18' rows='4' name='new_notes' id='new_notes' class="keypadfield"><?=$notes ? $notes : ''?></textarea>
</td>
</tr>
<tr>
<td><input type="button" name="reservenow" id="reservenow" value="Save" class="cmdbtn" />
<input type="button" name="newform" id="newform" value="New" class="cmdbtn" />
</td>
</tr>
</table>
</fieldset>
</td>
</tr>
</table>
<input type='hidden' id='new_guestid' name='new_guestid' value='<?=$guest_id?>' />
<input type='hidden' id='act' name='act' value='' />
</form>
<style>
body{font-family:lucida,arial,helvetica;}
fieldset {margin-top:10px;}
fieldset legend {border:1px solid #cccccc;background-color:#ececec;font-family:lucida,arial,helvetica;font-size:.7em}
#guestinfo td {padding:2px; border:1px solid #dddddd;}
#companyinfo td {padding:2px; border:1px solid #dddddd;}
#searchtable {empty-cells:show; border-collapse:false;border-spacing:0px;}
#guestreservetable {empty-cells:show; border-collapse:false;border-spacing:0px;width:100%}

#guestreservetable th{font-size:.7em;border-bottom:1px solid #333333;text-align:left}
#guestreservetable td{font-size:.7em;border-bottom:1px dotted #999999;}
#reserve-calendar {empty-cells:show; border-collapse:false;border-spacing:1px;padding:2px;width:100%}
#reserve-calendar td {width:40px;padding:2px;font-size:.7em;height:30px}
#reserve-calendar th {font-size:.7em;border:1px solid #00c;color:#ffffff;background-color:#006;}
#searchtable td {padding:2px; border-bottom:1px dotted #dddddd;font-size:.7em}
#searchtable  th {padding:2px; border-bottom:1px solid #51B1D8;font-size:.7em}
#guestinfo  label {font-size:.7em}
#companyinfo  label {font-size:.7em}
#searchcount   {font-size:.7em; color:#ff6600;margin-top:16px;}
.keypadfield{ border:1px solid #51B1D8; background-color:#E0EBEF; width:100%;font-size:.7em;font-family:lucida,arial,helvetica}
.numkeypadfield{ border:1px solid #51B1D8; background-color:#E0EBEF; width:100px;text-align:right;}
.keyselect {border:1px solid #51B1D8; background-color:#E0EBEF; width:100%}
.roomdate {border:1px solid #51B1D8;width:80px;font-size:.9em}
.cmdbtn {
	background-color:#95D5EF;
	border:1px solid #E0EBEF;
	width:80px;
	padding:4px;
	cursor:pointer;
}
.calendar-list {list-style:none;font-size:.7em;margin-left:-20px;}
.calendar-item {border:1px solid #51B1D8;margin-top:2px;padding:1px;padding-left:4px;width:30px;height:20px;}
.right {text-align:right}
.sunday, .saturday {
	border:1px solid #ffcc99;
}
.calendar-hdr {font-weight:bold;font-size:.7em;text-align:center;font-family:lucida,arial,helvetica;}
.next {float:right;}
.prev {float:left;}
.notmonth { border:1px solid #bbbbbb;}
.today {background-color:#ffccaa;}
.roomlist { list-style:none;}
.roomlist li { border:1px solid #E0EBEF;padding:2px;margin-top:2px;font-size:.7em;}
.error { border:1px solid #ff6600;}

#avlist { list-style:none;}
#avlist li {float:left;border:1px solid #95D5EF;padding:2px;margin:1px;}
#avlist li.reserved {float:left;border:1px solid #ff6600;}

</style>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.keypad.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.keypad.pack.js"></script>
<script lang="javascript">
$(document).ready(function(){
	//$(".numkeypadfield").keypad();
	//$(".keypadfield").keypad({keypadOnly:false, layout: $.keypad.qwertyLayout});
	$("#findcode").click(function(){document.location.href='reserve.php?code='+$("#new_reserve_code").val(); return false;});
	$("#newform").click(function(){document.location.href='reserve.php'; return false;});
	$("#mycal").datepicker({inline:true,dateFormat:'yy-mm-dd',changeYear:true,changeMonth:true,altField:'#selecteddate',altFormat:'yy-mm-dd'});
	$("#new_reserve_date").datepicker({dateFormat:'yy-mm-dd'});
	$(".roomdate").datepicker({dateFormat:'yy-mm-dd'});
	
	$("#new_roomtype").change(function() {
		$("#reserve-calendar td").removeClass("reserved");
		$("#new_theme").val(0);
		$("#new_status").val(0);
		$.post("reserve.php",{act:'rtselect', rtid: $(this).val()},function(ret) { $("#new_roomid").html(ret) });
	});
	$("#new_theme").change(function() {
		$("#reserve-calendar td").removeClass("reserved");
		$("#new_roomtype").val(0);
		$("#new_status").val(0);
		$.post("reserve.php",{act:'thselect', thid: $(this).val()},function(ret) { $("#new_roomid").html(ret) });
	});
	$("#searchgo").click(function(){
		$.post("guest.php", {act:'search', keyword: $("#searchkeyword").val() } , 
			function(resp) {
				$("#searchcount").html( resp.count + " guest(s) found. ");
				$("#searchtable thead").html(resp.head);
				$("#searchtable tbody").html(resp.body);
				$("a.launcher").click( 
						function() {
							var wcgst  = $(this).attr("alt"); 
							$.post('guest.php', {act:'specific', gid: wcgst}, function(resp) {
								$("#new_guestid").val(wcgst);
								$("#new_firstname").val(resp.data.firstname);
								$("#new_middlename").val(resp.data.middlename);
								$("#new_lastname").val(resp.data.lastname);
								return false;
							}, "json"  );
							$("#searchcontainer").hide();
						} 
					);
			},"json");
	});
	$("#viewtoday").click(function(){
		document.location.href='reserve.php?view=1';
		return false;
	});

	$("#verifyroom").click(function(){
		$.post("reserve.php", { act:'roomlist', status: $("#new_status").val(), wcdate:$("#mycal").val(), rtype: $("#new_roomtype").val(),theme:$("#new_theme").val() },
		function(ret){
			$("#roomlisting").html(ret);
			if($("#new_status").val()==0) {
				if($("#new_roomtype").val()==0 && $("#new_theme").val()==0) {
					$("#rllegend").html("Reservations for " +  $("#mycal").val() );
				}else{
					$("#rllegend").html("Available Rooms: " +  $("#mycal").val() );
				}
			}else{
				$("#rllegend").html($("#new_status").val() + " Rooms " + $("#mycal").val());	
			}
			$(".roomdate").datepicker({dateFormat:'yy-mm-dd'});
		});
		return false;
	}); 
	
	$("#cmdAdd").click(function(){
		$.post("reserve.php", {act:'guestadd',fn: $("#new_firstname").val(), mn:$("#new_middlename").val(), ln:$("#new_lastname").val()}, function(resp){$("#new_guestid").val(resp)});
	});
	$("#viewmonth").click(function(){
		var seldate = $("#mycal").val();
		var selarr = seldate.split("-");
		document.location.href="roomavail.php?mo="+selarr[1]+"&yr="+ selarr[0];
		return false;
	});
	
	$("#reservenow").click(function(){
		//alert($("#new_guestid").val() + $("#new_firstname").val() + $("#new_lastname").val() + $("#new_middlename").val());
		if($("#new_guestid").val()=="") {
			if($("#new_firstname").val()=="" && $("#new_lastname").val()=="" && $("#new_middlename").val()=="") {
				$("#new_firstname").addClass("error");
				$("#new_lastname").addClass("error");
				$("#new_middlename").addClass("error");
				return false;
			}else{
				$("#new_firstname").removeClass("error");
				$("#new_lastname").removeClass("error");
				$("#new_middlename").removeClass("error");
			}
		}else{
			$("#new_firstname").removeClass("error");
			$("#new_lastname").removeClass("error");
			$("#new_middlename").removeClass("error");
		}
		$("#act").val("newreserve");
		$("form").submit();
	});
	
	$("#guestreserve").click(function(){
		$.post('reserve.php',{act:'guestreserve', gid:$('#new_guestid').val(), fn:$("#new_firstname").val(), mn:$("#new_middlename").val() , ln:$("#new_lastname").val()},
		function(ret) {
			$("#roomlisting").html(ret);
			$("#rllegend").html("Found Reservations by  " + $("#new_firstname").val() + '  '+ $("#new_lastname").val());
		});
		return false;
	});
	
	$("#delroom").click(function(){
		$("#act").val("delroom");
		$("form").submit();
	});
});
</script>