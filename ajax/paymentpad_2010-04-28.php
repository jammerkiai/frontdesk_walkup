<?php

function displaypaymentpad($total=0) 
{
	$retval = "<fieldset id='paymentpad'>";
	//$retval.= "<legend >Balance Payment</legend>";
	$retval.= "<table id='paymenttable' width='100%'>";
	//$retval .= "<tr><td  class='orange'>Please pay this amount</td><td><input type='text'  id='newbalance' name='newbalance' value='' class='money' /></td></tr>";
	$retval .= "<tr><td>Cash Tendered</td><td align=right>";
	if("/fds/ajax/order.php" == curPageURL())
	{
		$retval .= "<input value='1' name='new_alreadypayed' id='new_alreadypayed' type='checkbox'><label for='new_alreadypayed'> Change returned </label>";
	}
	$retval .= "<input type='text' name='newcash' id='newcash' value='$total' class='money tender' />	
 </td></tr>";
	

	$retval .= "<tr><td class='change'>Total Change</td>
	<td align=right>
	 <input type='button' value='Clear' onclick='$(\"#newchange\").val(0)'/>&nbsp;
	<input type='text' id='newchange' name='newchange' style='width:100px;text-align:right;color:#CC00CC;font-weight:bold;border:0px' /></td></tr>";
	$retval .= "<tr><td>Card Payment</td><td align=right><input type='text' name='newcard' value='0'  id='newcard' class='money tender' /></td></tr>";
	$retval .= "<tr><td colspan=2>
		<fieldset id='carddetails' style='display:none;'><legend style='font-size:10px'>Card Payment Details</legend>
		<table>
		<tr><td>
		Card Type
		</td><td nowrap=true>
		<input type='radio' name='ctype' value='AMEX' id='ctype1' /><label for='ctype1'>AMEX</label>
		<input type='radio' name='ctype' value='JBE' id='ctype2' />
		<label for='ctype2'>JBE</label>		
		<input type='radio' name='ctype' value='Visa' id='ctype3' /><label for='ctype3'>Visa</label>
		<input type='radio' name='ctype' value='Mastercard' id='ctype4' />
		<label for='ctype4'>Mastercard</label>
		</td></tr>
		<tr><td><label for='newapproval'>Approval #</label></td>
		<td><input type='text' id='newapproval' name='newapproval' class='numfield'></td></tr>
		<tr><td><label for='newbatch'>Batch #</label></td>
		<td><input type='text' id='newbatch' name='newbatch' class='numfield' ></td></tr>
		</table>
		</fieldset>
	</td></tr>";
	$retval .= "<tr><td colspan=2>";
	$retval .= '
	<input type="button" class="denomination" value="1" />
	<input type="button" class="denomination" value="5" />
	<input type="button" class="denomination" value="10" />
	<input type="button" class="denomination" value="20" />
	<input type="button" class="denomination" value="50" />
	<input type="button" class="denomination" value="100" />
	<input type="button" class="denomination" value="500" />
	<input type="button" class="denomination" value="1000" />
	<input type="button" class="denomination" value="0.1" />
	<input type="button" class="denomination" value="0.01" />
	<input type="button" class="denomination half" value="Clear" />
		';
	$retval .= "</td></tr>";
	
	$retval .= "</table></legend>";

	$retval .= '<style>
	#paymentpad {font-family:lucida,arial,helvetica;}
	#paymentpad legend{
		font-size:11px;
		font-weight:bold;
		border:1px solid #cccccc;
		background-color:#efefef;
		padding-left:4px;
		color:#336600;}
	#paymentpad table td{font-size:11px;color:#6666FF}
	.denomination {}
	';
	return $retval;
}

function curPageURL() {
	$newstring = strstr($_SERVER["REQUEST_URI"], "?");	
	return str_replace($newstring, "", $_SERVER["REQUEST_URI"]);
}

?>