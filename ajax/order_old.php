<?php
/**
* order.php
* possible actions:
* - add new orderslip 
*		--> create orderslip, insert order details 
* - cancel item
* 		--> select item, create counter-record, negative amount
* - receive payment
*		--> select orderslip, display payment window
*		--> insert into payments 
*/
session_start();
include_once("config/config.inc.php");

/**(
* ajax part
*/

if($_GET["roomid"]) { //run on page load to get occupancy id
	$room = $_GET["roomid"];
	$sql = " select a.occupancy_id, a.actual_checkin, b.door_name, c.rate_name, a.shift_checkin, d.fullname
			from occupancy a, rooms b, rates c, users d
			where a.room_id=b.room_id and a.rate_id=c.rate_id and a.update_by=d.user_id and a.room_id=$room and actual_checkout='0000-00-00 00:00:00' ";
	$res = mysql_query($sql);
	list($occupancy, $checkindate,$doorname,$ratename,$shift,$fullname)=mysql_fetch_row($res);
}

if($_POST["act"]=="add") {
	$occupancy=$_POST["occupancy"];
	parse_str($_POST["data"]);
	list($categfrom, $thiscateg)=explode("_", $newsascateg);
	if($categfrom == 'm') {
		$table = "room_sales";
		$thisitem  =  $newsasitem;
	}elseif($categfrom == 'f') {
		$table = "fnb_sales";
		$thisitem = $newfooditem;
	}
	$now = date("Y-m-d H:i:s");
	//$thiscateg = getcategoryfromitem($newsascateg, $thisitem);
	
	$sql = "insert into $table (sales_date, occupancy_id, category_id, item_id,  unit_cost, qty  )
			values ('$now', '$occupancy', '$thiscateg' ,'$thisitem', '$newunitprice' ,'$qty' );
	";
	mysql_query($sql) or die($sql . mysql_error()) ; // save t this transaction
	
		
	echo gettransactions( $occupancy) ;
	//echo gettransactions(4, $occupancy) ;
	exit; //do not continue execution of rest of the script
}elseif($_POST["act"]=="cmd") {
	parse_str($_POST["data"]);
	$newstatus = $_POST["newstatus"];
	if($newstatus=='Confirm') {
		$thisstat = 'Confirmed';
	}elseif($newstatus=='Print'){
		$thisstat = 'Printed';
	}elseif($newstatus=='Cancel'){
		$thisstat = 'Cancelled';
	}elseif($newstatus=='Deliver'){
		$thisstat = 'Delivered';
	}
	if(is_array($cb_1) ) {
		$sas = implode(',', $cb_1);
		$sql = "update room_sales set status='$thisstat' where roomsales_id in ($sas) ";
		mysql_query($sql);
	}else{
		
	}
	if(is_array($cb_4) ) {
		$fnb = implode(',',$cb_4);
		$sql = "update fnb_sales set status='$thisstat' where fnbsales_id in ($fnb) ";
		mysql_query($sql);
	}
	
	echo gettransactions( $occupancy) ;
	exit;
}

function gettransactions( $occupancy, $status=" 'Cancelled' ") {
	$typearr = array(1, 4);
	$retval = "";
	$grandtotal = 0;
	$retval.="<tr >";
	$retval .= "<td class='hdr'><input type='checkbox'  onclick='  $(\".cbitem\").attr(\"checked\",  $(this).attr(\"checked\") )' /></td>";
	$retval .= "<td class='hdr'>Date</td>";
	//$retval .= "<td class='hdr'>Category</td>";
	$retval .= "<td class='hdr'>Item</td>";
	$retval .= "<td class='hdr '>Unit Cost</td>";
	$retval .= "<td class='hdr '>Quantity</td>";
	$retval .= "<td class='hdr '>Total Cost</td>";
	$retval .= "<td class='hdr '>Status</td>";
	$retval.="</tr>";
	foreach($typearr as $type) {
	if($type==1) {
	// extract from room_sales
		$sql = " select  a.roomsales_id, a.sales_date, b.sas_cat_name, c.sas_description, a.unit_cost, a.qty ,a.status
				from room_sales  a, sas_category b, sales_and_services c
				where a.item_id=c.sas_id and b.sas_cat_id=c.sas_cat_id  and a.occupancy_id='$occupancy' and a.status not in ('Cancelled') 
				and b.sas_cat_id < 3
				order by a.sales_date 
				";
		$typeheader = "Misc";
	}elseif($type==4) {
		$sql = " select  a.fnbsales_id, a.sales_date, b.food_category_name, c.fnb_name, a.unit_cost, a.qty ,a.status
				from fnb_sales  a, food_categories b, fnb c
				where a.item_id=c.fnb_id and a.category_id=c.food_category_id and b.food_category_id=c.food_category_id and a.occupancy_id='$occupancy'
				 and a.status not in ( 'Cancelled' )
				order by a.sales_date 
				";
		$typeheader = "FnB";
	}
	$cbname = "cb_$type" . '[]';
	$res = mysql_query($sql) or die($sql. mysql_error());
	
	
	if(mysql_num_rows($res)) {
		$oldcateg = "";
		$total = 0;
	
		$retval .= "<tr><td colspan=7 class='hdr'>$typeheader</td></tr>";
		
		while(list($sid, $date,$catname,$itemname,$unitcost, $qty,$status) = mysql_fetch_row($res)) {
			if($oldcateg != $catname) {
				$retval .= "<tr><td colspan=7 class='subhdr'>$catname</td></tr>";
			}
			$retval .= "<tr>";
			$retval .= "<td><input type='checkbox' name='$cbname' class='cbitem' value='$sid'/></td>";
			$retval .= "<td>$date</td>";
			//$retval .= "<td>$catname</td>";
			$retval .= "<td>$itemname</td>";
			$retval .= "<td class='number'>$unitcost</td>";
			$retval .= "<td class='number'>$qty</td>";
			$retval .= "<td class='number'>" . number_format($unitcost * $qty,2) ."</td>";
			$retval .= "<td class='number'>$status</td>";
			$retval .= "</tr>";
			$oldcateg = $catname;
			$total += $unitcost * $qty;
			$grandtotal += $unitcost * $qty;
		}
		$retval.="<tr><td class='number hdr' colspan=6>Sub-total:</td><td class='number hdr'>". number_format($total,2) ."</td></tr>";
		//$retval .= "<tr><td colspan=7 >&nbsp;</td></tr>";
	}
	}
	$retval.="<tr><td class='number hdr' colspan=6>Grand Total:</td><td class='number hdr'>". number_format($grandtotal,2) ."</td></tr>";
	return $retval;
}

function getcategoryfromitem($cat, $item) {
	if($cat==1) {
		$sql = " select sas_cat_id from sales_and_services where sas_id='$item' ";
	}elseif($cat==4){
		$sql = " select food_category_id from fnb where fnb_id='$item' ";
	}
	$res = mysql_query($sql);
	list($cid)=mysql_fetch_row($res);
	return $cid;
}

function getitemfromcat($cat="", $itemid="") {
	if($cat == 1) { //extract from sas
		$sql = " select sas_description from sales_and_services where sas_id='$itemid' ";
	}elseif($cat==4){
		$sql = " select fnb_name from fnb where fnb_id='$itemid' ";
	}
	$res = mysql_query($sql);
	if(mysql_num_rows($res)) {
		list($name) =mysql_fetch_row($res);
		return $name;
	}else{
		return '';
	}
}

/**
*
* display part
*/
function getfoodcategories($name="",$selected="",$onchange="") {
	$sql  = "select food_category_id, food_category_name from food_categories order by food_category_name";
	$res = mysql_query($sql);
	//$retval = "<select name='$name'  id='$name' ";
	//if($onchange) $retval.= " onchange='$onchange' ";
	//$retval.=">";
	//$retval.="<option></option>";
	while(list($fid, $fname)=mysql_fetch_row($res)) {
		$retval.="<option value='f_$fid' alt='fnb' ";
		$retval.= ($fid==$selected) ? " selected " : "";
		$retval.= ">$fname</option>";
	}
	//$retval.="</select>";
	return $retval;
}

function getfooditems($name="",$selected="",$onchange="") {
	$sql = " select food_categories.food_category_id, food_categories.food_category_name, 
			fnb.fnb_id, fnb.fnb_code, fnb.fnb_name, fnb.fnb_price
			from fnb left join food_categories on fnb.food_category_id=food_categories.food_category_id
			order by food_categories.food_category_name, fnb_name
			";
	$res = mysql_query($sql) or die($sql);

	$retval="<select name='$name' id='$name' ";
	$retval.= ">";
	$retval.="<option></option>";
	$oldcateg='';
	while(list($fcid, $fcname,$fnbid,$fnbcode,$fnbname,$fnbprice)=mysql_fetch_row($res)) {
		if($oldcateg != $fcid) {
			$retval.= ($oldcateg=="")  ? "" : "</optgroup>";
			$retval.="<optgroup label='$fcname' id='fcat_f_$fcid' class='foodopts'>\n";
		}
		$retval.="<option value='$fnbid' alt='$fnbprice' ";
		$retval.= ($fnbid==$selected) ? " selected " : "";
		$retval.= ">$fnbname</option>\n";
		$oldcateg=$fcid;
	}
	$retval.="</optgroup>";
	$retval.="</select>";
	return $retval;
}

function getsascategories($name="",$selected="",$onchange="") {
	$sql  = "select * from sas_category where sas_cat_id < 3 order by sas_cat_name";
	$res = mysql_query($sql);
	//$retval = "<select name='$name'  id='$name' ";
	//if($onchange) $retval.= " onchange='$onchange' ";
	//$retval.=">";
	//$retval.="<option></option>";
	$retval="";
	while(list($fid, $fname)=mysql_fetch_row($res)) {
		$retval.="<option value='m_$fid'  alt='misc' ";
		$retval.= ($fid==$selected) ? " selected " : "";
		$retval.= ">$fname</option>";
	}
	//$retval.="</select>";
	return $retval;
}

function getsasitems($name="",$selected="",$onchange="") {
	$sql = " select sas_category.sas_cat_id, sas_category.sas_cat_name, 
			sales_and_services.sas_id, sales_and_services.sas_description, sales_and_services.sas_amount
			from sales_and_services left join sas_category on sales_and_services.sas_cat_id=sas_category.sas_cat_id
			where sas_category.sas_cat_id < 3 order by sas_category.sas_cat_name, sales_and_services.sas_description ;
			";
	$res =mysql_query($sql) or die($sql. mysql_error());

	$retval="<select name='$name' id='$name' ";
	$retval.= ">";
	$retval.="<option></option>";
	$oldcateg='';
	while(list($fcid, $fcname,$fnbid,$fnbname,$fnbprice)=mysql_fetch_row($res)) {
		if($oldcateg != $fcid) {
			$retval.= ($oldcateg=="")  ? "" : "</optgroup>";
			$retval.="<optgroup label='$fcname' id='mcat_m_$fcid' class='foodopts'>\n";
		}
		$retval.="<option value='$fnbid'  alt='$fnbprice' ";
		$retval.= ($fnbid==$selected) ? " selected " : "";
		$retval.= ">$fnbname</option>\n";
		$oldcateg=$fcid;
	}
	$retval.="</optgroup>";
	$retval.="</select>";
	return $retval;
}

function dropcounter($name, $selected) {
	$retval ="<select name='$name' id='$name'>";
	for($x=0; $x <=20; $x++) {
		$retval.="<option value='$x'>$x</option>";
	}
	$retval.="</select>";
	return $retval;
}

$fooditems = getfooditems("newfooditem");
$sasitems = getsasitems("newsasitem");
$existing = gettransactions($occupancy) ;
$counter = dropcounter('newqty',0);
//$existing .= gettransactions(4, $occupancy) ;
?>
<style>
#newsascateg,#newfooditem,#newsasitem,#dummy,#newqty {
	height:24px;
	font-size:1em;
	color:#0000ff;
	background-color:#eeeeee;
	font-weight:bold;
	width:240px;
}

#orderset legend {
	border:1px solid #cccccc;
	font-family:verdana, arial, helvetica;
	font-size:.7em;
}

#orderslist {
	font-family:verdana,arial,helvetica;
	font-size:.7em;
	border-collapse:collapse
}

#orderslist td.hdr {
	font-size: 1em;
	font-weight:bold;
	border-bottom:1px solid #eeeeee;
	text-align:left;
	//background-color:#9BD1E6;
	background-color:#eeeeee;
	padding:4px;
}

#orderslist td {
	border-bottom:1px dotted #cccccc;
	padding:2px;
}

#orderslist td.number {
	border-bottom:1px dotted #cccccc;
	text-align:right;
	padding:2px;
}
#orderslist td.subhdr {
	font-weight: bold;
	border-bottom: 1px solid #43b7c4;
}

#cmdmenu {
	list-style:none;
	margin-left:-40px;
}

#cmdmenu li{
	float:left;
	width:100px;
}

.cmdbtn {
	width:100px;
	border:1px solid  #ffffff;
	background-color: #9BD1E6;
	margin-right:2px;
	padding:2px;
	cursor:pointer;
}

</style>
<div>
<form method="post" action=""  id="myform">
<fieldset id="orderset">
<legend>Select Order Items Here:</legend>
<table width="600" cellpadding="1" cellspacing="1" border="0">
<tbody>
<tr>
<td>
<select id="newsascateg"  name="newsascateg">
<option value="0">Select category...</option>
	<optgroup label="Miscellaneous">
		<?=getsascategories('mymiscateg')?>
	</optgroup>
	<!--option value="2">Cooperative</option-->
	<optgroup label="Food and Services">
		<?= getfoodcategories("myfoodcateg");?>
	</optgroup>
</select>
</td>
<td>
<select id="dummy"></select>
<?= $fooditems . $sasitems?></td>
<td><input type="text" name="qty" id="qty" value="1" size="2" maxlength="2" />
<input type="button"  name="add"  value="Add"  id="add" />
</td>
</tr>
</tbody>
</table>
</fieldset>
<ul id="cmdmenu">
<li><input type="button" name="cmdbtn" id="cmdbtn1" value="Confirm"  class='cmdbtn' /></li>
<li><input type="button" name="cmdbtn" id="cmdbtn2" value="Print"  class='cmdbtn' /></li>
<li><input type="button" name="cmdbtn" id="cmdbtn3" value="Cancel"  class='cmdbtn' /></li>
</ul>
<div style="clear:both;"></div>
<table id="orderslist"  width="600" cellpadding="1" cellspacing="1" border="0">
<tbody>
<?=$existing ?>
</tbody>
</table>
<input type="hidden" id="newunitprice" name="newunitprice" />
<input type="hidden" id="occupancy" name="occupancy" value="<?=$occupancy?>" />
<input type="hidden" id="roomid" name="roomid" value="<?=$roomid?>" />
</form>
</div>
<script src="../js/jquery.js" type="text/javascript"></script>
<script lang="javascript">
	$(document).ready(function(){
		/*
		$(".foodopts").hide();
		$("#newfooditem :first").attr("selected","selected");
		$("#mycateg").change(function(){ 
			$(".foodopts").hide();
			$("#cat_" + $(this).val()).show(); 
			$("#cat_" + $(this).val() + " option:first").attr("selected","selected"); 
		});
		*/
		$("#newsasitem").hide();
		$("#newfooditem").hide();
		$("#newfooditem").change(function() {
			$("#newunitprice").val($("#newfooditem option:selected").attr('alt') );
		});
		$("#newsasitem").change(function() {
			$("#newunitprice").val($("#newsasitem option:selected").attr('alt') );
		});
		
		$("#newsascateg").change(function(){
			var alt = $("#newsascateg option:selected").attr('alt');
			$("#newsasitem").hide();
			$("#newfooditem").hide();
			$("#dummy").hide();
			if(alt=='fnb') {
				$("#newfooditem").show();
				$(".foodopts").hide();
				$("#fcat_" + $(this).val()).show(); 
				//$("#fcat_" + $(this).val() + " option:first").attr("selected","selected"); 
			}else if(alt=='misc') {
				$("#newsasitem").show();
				$(".foodopts").hide();
				$("#mcat_" + $(this).val()).show(); 
				//$("#mcat_" + $(this).val() + " option:first").attr("selected","selected"); 
			}else{
				$("#dummy").show();
			}	
			
			
		});
		$("#add").click(function(){
			$.post("order.php",{ act:'add', occupancy: <?=$occupancy?>, data: $("#myform").serialize() }, function(resp) {
				$("#orderslist tbody").html(resp);
			});
			return false;
		});
		$(".cmdbtn").click(function(){
			$.post("order.php",{ act:'cmd', newstatus: $(this).val(),occupancy: <?=$occupancy?>, data: $("#myform").serialize() }, function(resp) {
				$("#orderslist tbody").html(resp);
			});
			return false;
		});
	})
</script>