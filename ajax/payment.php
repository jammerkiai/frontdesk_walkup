<fieldset id="paymentset"><legend style=''>Balance Payment</legend>
	<table width="300">
	<tr><td  class='orange'>Please pay this amount</td><td><input type='text'  id='newbalance' name='newbalance' value='<?=$grandtotal?>' class='money' /></td></tr>
	<tr><td>Cash Tendered</td><td><input type='text' name='newcash' id='newcash' value='0' class='money' /></td></tr>
	<tr><td>Card Payment</td><td><input type='text' name='newcard' value='0'  id='newcard' class='money' /></td></tr>
	<tr><td colspan=2>
		<fieldset><legend>Card Payment Details<legend>
		<table>
		<tr><td>
		Select card type
		</td><td>
		<input type='radio' name='ctype' value='Visa' id='ctype1' /><label for='ctype1'>Visa</label><br />
		<input type='radio' name='ctype' value='Mastercard' id='ctype2' /><label for='ctype2'>Mastercard</label></td></tr>
		<tr><td><label for=''>Trans#</label></td><td><input type='text' name='newtrxn' ></td></tr>
		<tr><td><label for=''>Batch#</label></td><td><input type='text' name='newbatch' ></td></tr>
		</table>
		</fieldset>
	</td></tr>
	<tr><td class='change'>Total Change</td><td><input type='text' id='newchange' value='$change' name='newchange' value='0'  class='money' /></td></tr>
	</table></legend>
	<ul id='cmdmenu'>
	<li><input type="button" name="cmdbtn" id="cmdbtn1" value="Re-compute"  class="cmdbtn" /></li>
	<li><input type="button" name="cmdbtn" id="cmdbtn2" value="Pay Balance"  class="cmdbtn" /></li>
	</ul>
<style>
h1 { text-align:center;font-size:1em;font-family:verdana, arial, helvetica;}
#paymentset table {font-family:lucida,arial,helvetica}
#paymentset table td {font-size:.6em} 
#paymentset table th {font-size:.7em;font-weight:bold;text-align:left;}

.x-footer{bottom:0;left:10;font-size:.6em}

#paymentset legend {
	font-size:1.2em;font-weight:bold;border:1px solid #cccccc;background-color:#efefef;padding:2px;color:#9BD1E6;
}
#soatable td{
	border-bottom:1px dotted #cccccc;
}

#cmdmenu {
	list-style:none;
	margin-left:-40px;
}

#cmdmenu li{
	float:left;
	width:100px;
}

.cmdbtn {
	width:100px;
	border:1px solid  #ffffff;
	background-color: #9BD1E6;
	margin-right:2px;
	padding:2px;
	cursor:pointer;
}
.money {
	width:40;
	text-align:right;
}
#paymenttable {
	font-family:lucida, arial, helvetica;
}
#paymenttable td {
	font-weight:normal;font-size:.7em;
}

#paymenttable td.orange {
	color:#ff6600;font-weight:bold;
}

#paymenttable td.change {
	color:#007700;font-weight:bold;
}

</style>