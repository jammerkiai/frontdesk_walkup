Ext.ns('Application');
Application.SimpleCheckin = Ext.extend(Ext.form.FormPanel, {
	frame:false,
	border:false,
	id:'checkinform',
	autoScroll:true,
    initComponent:function() {
		Ext.apply(this, Ext.apply(this.initialConfig));
		Ext.apply(this, {
			store : new Ext.data.JsonStore({
				autoDestroy:true,
				url: "./ajax/json.php",
				batch:true,
				method: "POST", 
				baseParams: {
					act:'roomrates', owner: this.id, roomtype: this.roomtype
				}
			})
		});
        Application.SimpleCheckin.superclass.initComponent.apply(this, arguments);

    } 

    ,onRender:function(ct,position) {
		Application.SimpleCheckin.superclass.onRender.apply(this, arguments);
		
		this.store.load();
		this.store.on({
			'load' : {
				fn:function(r) {
					var myform = Ext.getCmp(r.baseParams.owner);
					var fs = new  Ext.Panel({	
						id:'rateselector',
						height:100,
						layout:'hbox',
						layoutConfig: {
							padding:'5 0 5 0',
							align:'stretch'
						},
						defaults:{
							flex:1,
							width:100,
							margins:'0 5 0 0',
                            pressed: false,
                            toggleGroup:'btns',
                            allowDepress: false,
							handler: function(o) {
								Ext.getCmp('totalcost').setValue(o.rate);
								fs2.show();
								var store2 = new Ext.data.JsonStore(
								{
									autoDestroy:true,
									url: "./ajax/json.php",
									batch:true,
									method: "POST", 
									baseParams: {
										act:'roomdiscounts', owner: this.id, roomtype: o.roomtype, rate: o.rateid
									},
									autoLoad: true,
									listeners: {
										load: {
											fn: function(r){	
												fs2.removeAll();
												r.each(function(r) {
													fs2.add( new Ext.Button({
														enableToggle:true,
														toggleGroup:'btns3',
														allowDepress: false,
														pressed:false,
														text: r.data.discount_label,
														discount: r.data.discount_percent,
														handler: function(o) {
															alert(o.discount);
															var currenttotal = Ext.getCmp('totalcost').getValue();
															var totaldiscount = currenttotal * o.discount;
															var newtotal = currenttotal - totaldiscount;
															Ext.getCmp('totalcost').setValue(newtotal);
															Ext.getCmp('totaldiscount').setValue(totaldiscount);
														}
													}) );
												});
												
												fs2.add({
														xtype:'button', 
														flex:1,
														text:'Other',
														width:100,
														margins:'0 5 0 0',
														toggleGroup:'btns3',
														enableToggle:false,
														allowDepress: false,
														pressed:false,
														handler:function() {
															//oic.show();
														}
													});
													
												fs2.doLayout();
											},
											scope:this
										}
									}
								}
								);
							}
						}
					});
					
					var oic = new Ext.Panel(
					{
						id:'oicauthform',
						layout:'form',
						defaultType:'textfield',
						defaults:{labelStyle:'width:180'},
						items:[
							{
								fieldLabel:'OIC Password',
								inputType:'password',
								allowBlank:false,
								blankText:'Please enter you password'
							},
							{
								fieldLabel:'Enter Discount (%)',
								allowBlank:false,
								blankText:'Please enter the discount'
							},
							{
								fieldLabel:'Remarks',
								blankText:'Please indicate reason for the discount'
							}
						]
					}
					);
					
					r.each(function(r){
						//Ext.getCmp(this.store.baseParams.owner).add({
							fs.add({
							xtype:'button',
							margin:'2 2 2 2',
							text:r.data.rate_name,
							rate: r.data.amount,
							rateid: r.data.rate_id,
							roomtype: r.data.room_type_id
						});
					});
					
					var fs2 = new  Ext.Panel({
						id:'discountselector',
						layout:'hbox',
						height:100,
						layoutConfig: {
							padding:'5 0 5 0',
							align:'stretch'
						},
						defaults:{
							flex:1,
							width:100,
							margins:'0 5 0 0',
                            pressed: false,
                            toggleGroup:'btns2',
                            allowDepress: false	
							}
					});
					
					var summary = new Ext.Panel({
						autoHeight:true,
						layout:'form',
						defaults:{labelStyle:'width:180'},
						items:[
							{
								xtype:'textfield',
								name:'Room Charge',
								id:'roomcharge',
								style:'text-align:right',
								fieldLabel:'Total Room Charge'
							},
							{
								xtype:'textfield',
								name:'totaldiscount',
								id:'totaldiscount',
								style:'text-align:right',
								fieldLabel:'Total Discount'
							},
							{
								xtype:'textfield',
								name:'totalcost',
								id:'totalcost',
								style:'text-align:right',
								fieldLabel:'Please pay this amount'
							},
							{
								xtype:'textfield',
								name:'cashpayment',
								style:'text-align:right',
								fieldLabel:'Cash Payment'
							}
						]
					});
					
					fs.add({
						title:'Hrs Extension'
						,frame:true
						,items:[{
							xtype:'combo',
							store: [[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11]],
							name:'hrsextend',
							typeAhead: true,
							mode: 'local',
							forceSelection: true,
							triggerAction: 'all',
							emptyText:'Extension...',
							selectOnFocus:true
						}]
					}	 );
						
					var panel = new Ext.Panel({
						id:'main-panel',
						baseCls:'x-plain',
						layout:'table',
						layoutConfig: {columns:2},	
						// applied to child components
						defaults: {frame:true, margins:'5 0 5 0'},
						items:[
						{
							title:'Select Room Rate',
							width:640,
							height:140,
							colspan:2,
							items:fs
						}	
						,
						{
							title:'Discount',
							width:320,
							colspan:1,
							height:140,
							
							items:fs2
							
						},{
							title:'Adjustment',
							width:320,
							colspan:1,
							height:140,
							items:oic
						}
						,
						{
							title:'Payment Options'
						},{
							title:'Summary of Charges',
							width:410,
							colspan:2,
							items:summary
						}]
					});	
					
					
				
					myform.add(panel);
					myform.doLayout();
				}//end fn
				,scope:this
				
			}//end onRender
			
		});
		
	}
});
Ext.reg('simplecheckin', Application.SimpleCheckin);
