Application.QuickStatGrid = Ext.extend(Ext.grid.GridPanel, {
     border:false
    ,initComponent:function() {
		var statSel = new Ext.form.ComboBox({
			store: new Ext.data.ArrayStore({
				fields:['status_id','status_name'],
				data: [[1, 'Available'],[2, 'Occupied'],[3, 'Cleaning'],[4, 'Maintenance']]
			}),
			displayField:'status_name',
			valueField:'status_id',
			typeAhead: true,
			mode: 'local',
			forceSelection: true,
			triggerAction: 'all',
			emptyText:'Select status...',
			selectOnFocus:true,
			listeners:{
				select : {
					fn: function(c, r, i) {
						store.reload({params:{status: r.data.status_id}})
					},
					scope: this
				}
			}
		});

		var reader = new Ext.data.JsonReader(
			{
				root:'rooms',
				idProperty:'room_id'
			},
			[{name: 'room_id'},
		   {name: 'door_name'},
		   {name: 'floor_label'},
		   {name:'room_type_name'},
		   {name: 'status_name'},
		   {name: 'last_update', type: 'date', dateFormat: 'Y-m-d H:i:s'}]
		); 
		var store = new Ext.data.GroupingStore({
				reader:reader,			
				url:'ajax/quickstat-data.php',
				params:{ status: 1},
				sortInfo:{field: 'room_type_name', direction: "ASC"},
				groupOnSort: true,
				remoteGroup: true,
				groupField: 'room_type_name',
				fields:[{name: 'room_id'},
					   {name: 'door_name'},
					   {name: 'floor_label'},
					   {name: 'room_type_name'},
					   {name: 'status_name'},
					   {name: 'last_update', type: 'date', dateFormat: 'Y-m-d H:i:s'}
					   ],
				root:'rooms',
				idProperty:'room_id'
			});
			
        var config = {
			title:'Room Status View',
			collapsible:true,
            store: store,
			columns: [
				{id:'room_id',header:"RoomID",dataIndex:'room_id',hidden:true},
				{header: "Floor", width: 30, sortable: true, dataIndex: 'floor_label'},
				{header: "Room Number", width: 20, sortable: true, dataIndex: 'door_name'},
				{header: "Room Type", width: 30, sortable: true, dataIndex: 'room_type_name'},
				{header: "Status", width: 30, sortable: true, dataIndex: 'status_name'},
				{header: "Last Updated", width: 30, sortable: true, renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s'), dataIndex: 'last_update'}
			],
			
			view: new Ext.grid.GroupingView({
				forceFit: true,
				startCollapsed:true,
				hideGroupedColumn:true,
				showGroupName:false,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Rooms"  : "Room" ]})'
			}),
			tbar: [statSel],
			animCollapse: false,
			iconCls: 'x-icon-dooropen'
        }; 
 
        Ext.apply(this, Ext.apply(this.initialConfig, config)); 
        Application.QuickStatGrid.superclass.initComponent.apply(this, arguments);
    } 
 
    ,onRender:function() { 
		this.store.load(),
        Application.QuickStatGrid.superclass.onRender.apply(this, arguments);
    } 
});
 
Ext.reg('quickstatgridpanel', Application.QuickStatGrid);

Application.AuthorizationForm = Ext.extend(Ext.form.FormPanel, {
     border:false
	,id:'statusform'
    ,initComponent:function() {
		//var keyboardplugin = new Ext.ux.plugins.VirtualKeyboard();
		
        var config = {
			labelWidth: 80, 
			labelAlign:'top',
			frame:false,
			border:false,
			bodyStyle:'padding:15px 15px 10px 10px',
			defaults: {width: 200},
			defaultType: 'textfield',
			items: [
			{
				id:'newremarks',
				name:'newremarks',
				inputType:'textarea',
				fieldLabel:'Notes/Remarks'
			},
			{
				id:'roomid',
				name:'roomid',
				inputType:'hidden',
				value:this.roomid
			}
			]
        }; // eo config object
 
        // apply config
        Ext.apply(this, Ext.apply(this.initialConfig, config));
 
        Application.AuthorizationForm.superclass.initComponent.apply(this, arguments);
    } // eo function initComponent
 
    ,onRender:function() {
        Application.AuthorizationForm.superclass.onRender.apply(this, arguments);
		
		var statSel = new Ext.form.ComboBox({
			store: new Ext.data.ArrayStore({
				fields:['status_id','status_name'],
				data: [[1, 'Available'],[2, 'Occupied'],[3, 'Cleaning'],[4, 'Maintenance']]
			}),
			fieldLabel:'Set new status as...',
			displayField:'status_name',
			valueField:'status_id',
			typeAhead: true,
			mode: 'local',
			forceSelection: true,
			triggerAction: 'all',
			emptyText:'Select status...',
			selectOnFocus:true,
			name:'newstatus',
			hiddenName:'status_id',
			id:'newstatus'
		});
		
		this.add(statSel);
		this.doLayout();
    } // eo function onRender
});
 
Ext.reg('authorizationform', Application.AuthorizationForm);